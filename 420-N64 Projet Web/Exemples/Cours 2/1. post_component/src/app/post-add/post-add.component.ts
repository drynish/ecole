import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-post-add',
  imports: [FormsModule],
  templateUrl: './post-add.component.html',
  styleUrl: './post-add.component.css'
})
export class PostAddComponent {
  newPost = { title: '', body: '' };

  // Méthode pour ajouter un nouveau post
  onSubmit() {
    if (this.newPost.title && this.newPost.body) {
      // Ajouter le post au tableau
      this.posts.push({ ...this.newPost });
      // Réinitialiser le formulaire
      this.newPost = { title: '', body: '' };
    }
  }

  // Tableau de posts accessible dans le composant
  posts = [
    { title: 'Premier post', body: 'Ceci est le contenu du premier post.' },
    { title: 'Deuxième post', body: 'Voici un autre post avec du contenu.' }
  ];
}
