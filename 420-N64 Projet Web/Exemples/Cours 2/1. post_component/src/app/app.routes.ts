import { Routes } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';
import { PostAddComponent } from './post-add/post-add.component';

export const routes: Routes = [ 
    { path: 'posts', component: PostListComponent },
    { path: 'add-post', component: PostAddComponent },
    { path: '', redirectTo: '/posts', pathMatch: 'full' }];
