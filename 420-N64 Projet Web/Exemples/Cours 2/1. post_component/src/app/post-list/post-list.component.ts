import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-post-list',
  imports: [CommonModule],
  templateUrl: './post-list.component.html',
  styleUrl: './post-list.component.css'
})
export class PostListComponent {
  posts = [
    { title: 'Premier post', body: 'Ceci est le contenu du premier post.' },
    { title: 'Deuxième post', body: 'Voici un autre post avec du contenu.' }
  ];
}
