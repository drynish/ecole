import { Component } from '@angular/core';
import { PostAddComponent } from './post-add/post-add.component';
import { PostListComponent } from './post-list/post-list.component';

@Component({
  standalone: true,
  selector: 'app-root',
  imports: [PostAddComponent, PostListComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent {
  title = 'Blog';
}
