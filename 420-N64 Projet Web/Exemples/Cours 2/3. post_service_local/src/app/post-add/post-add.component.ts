import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PostService } from '../post.service';

@Component({
  standalone: true,
  selector: 'app-post-add',
  imports: [FormsModule],
  templateUrl: './post-add.component.html',
  styleUrl: './post-add.component.css'
})
export class PostAddComponent {
  newPost = { title: '', body: '' };

  constructor(private postService: PostService) { }

  onSubmit() {
    if (this.newPost.title && this.newPost.body) {
      //const postToAdd = { ...this.newPost } ;
      this.postService.addPost(this.newPost);

      this.newPost.body = '';
      this.newPost.title = '';
    }
  }

}
