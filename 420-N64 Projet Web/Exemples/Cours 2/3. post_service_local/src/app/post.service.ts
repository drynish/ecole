import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  
  private posts = new BehaviorSubject<{ title: string, body: string }[]>([
    { title: 'Mon premier post', body: 'Ceci est le contenu du post' },
    { title: 'Mon deuxième post', body: 'Ceci est le contenu du post 2' }
  ]);

  // Expose posts$ comme un Observable pour permettre aux composants de s'abonner
  posts$ = this.posts.asObservable();

  constructor() { }

  addPost(post: { title: string, body: string }) {
    console.log("Here")
    const currentPosts = this.posts.getValue();
    this.posts.next([...currentPosts, post]);
    console.log(this.posts.getValue());
  }

}
