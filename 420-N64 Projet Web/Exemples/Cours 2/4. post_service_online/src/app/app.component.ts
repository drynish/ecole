import { Component } from '@angular/core';
import { PostListComponent } from './post-list/post-list.component';

@Component({
  standalone: true,
  selector: 'app-root',
  imports: [ PostListComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent {
  title = 'Blog';
}
