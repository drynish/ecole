import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostService } from '../post.service';

@Component({
  standalone: true,
  selector: 'app-post-list',
  imports: [CommonModule],
  templateUrl: './post-list.component.html',
  styleUrl: './post-list.component.css',
})
export class PostListComponent implements OnInit {

  posts: { title: string, body: string }[] = [];
  
  constructor(private postService: PostService) {};

  ngOnInit() {
    this.postService.getPosts().subscribe((posts) => {
      this.posts = posts;
      console.log(this.posts);  // Vérifie dans la console si tu reçois les données
    });
  }
}
