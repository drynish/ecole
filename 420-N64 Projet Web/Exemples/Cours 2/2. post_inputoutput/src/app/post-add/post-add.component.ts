import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  standalone: true,
  selector: 'app-post-add',
  imports: [FormsModule],
  templateUrl: './post-add.component.html',
  styleUrl: './post-add.component.css'
})
export class PostAddComponent {
  @Output() postAdded = new EventEmitter<{ title: string, body: string }>();

  newPost = { title: '', body: '' };

  // Méthode pour ajouter un nouveau post
  onSubmit() {
    if (this.newPost.title && this.newPost.body) {
      // Ajouter le post au tableau
      this.postAdded.emit(this.newPost); // Envoie le post au parent
      // Réinitialiser le formulaire
      this.newPost = { title: '', body: '' };
    }
  }

}
