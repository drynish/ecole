import { Component } from '@angular/core';
import { PostAddComponent } from './post-add/post-add.component';
import { PostListComponent } from './post-list/post-list.component';

@Component({
  standalone: true,
  selector: 'app-root',
  imports: [PostAddComponent, PostListComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  posts = [
    { title: 'Premier post', body: 'Ceci est le corps du premier post' },
    { title: 'Deuxième post', body: 'Ceci est le corps du deuxième post' }
  ];

  // Fonction pour ajouter un post à la liste
  onPostAdded(newPost: { title: string, body: string }) {
    this.posts.push(newPost);
  }
}
