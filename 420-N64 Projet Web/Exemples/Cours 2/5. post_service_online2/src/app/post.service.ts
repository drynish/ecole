import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PostService {
  
  private baseUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) {}

  getPosts(page: number, size: number): Observable<{ posts: { title: string; body: string }[], total: number }> {
    return this.http.get<{ title: string; body: string }[]>(this.baseUrl).pipe(
      map((allPosts) => {
        const total = allPosts.length; // Nombre total d'éléments
        const startIndex = (page - 1) * size;
        const endIndex = startIndex + size;
        const posts = allPosts.slice(startIndex, endIndex); // Pagination simulée
        return { posts, total };
      })
    );
  }

}
