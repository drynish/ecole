import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { FormsModule } from '@angular/forms';

@Component({
  standalone: true,
  selector: 'app-post-list',
  imports: [CommonModule, FormsModule],
  templateUrl: './post-list.component.html',
  styleUrl: './post-list.component.css',
})
export class PostListComponent implements OnInit {

  posts: { title: string; body: string }[] = [];
  totalPosts = 0;
  totalPages = 0;
  page = 1;
  size: number = 5; // Nombre de posts par page
  sizes: number[] = [5, 10, 15, 20]; // Options pour la taille des pages
  
  constructor(private postService: PostService) {};

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts() {
    this.postService.getPosts(this.page, this.size).subscribe((response) => {
      this.posts = response.posts;
      this.totalPosts = response.total;
      this.totalPages = Math.ceil(this.totalPosts / this.size); // Calcul du nombre total de pages
    });
  }

  nextPage() {
    if (this.page < this.totalPages) {
      this.page++;
      this.loadPosts();
    }
  }

  prevPage() {
    if (this.page > 1) {
      this.page--;
      this.loadPosts();
    }
  }
}
