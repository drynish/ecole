import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
import dotenv from 'dotenv'; // Import dotenv

import router from './routes/todoRoutes';


dotenv.config();

const app = express();

// Middleware
app.use(cors());
app.use(bodyParser.json());

// Routes
app.use('/api', router);

// MongoDB Connection
const mongoURI = process.env.MONGODB_URI || 'mongodb://localhost:27017/mean-stack'; 
mongoose.connect(mongoURI).then(() => {
  console.log('Connected to MongoDB');
}).catch((err) => {
  console.error('MongoDB connection error:', err);
  process.exit(1); 
});

const PORT = process.env.PORT || 3000; 
const server = app.listen(PORT, () => { 
  console.log(`Server running on http://localhost:${PORT}`);
});


process.on('SIGINT', () => {
  console.log('Shutting down server...');
  server.close(() => { // Close the server gracefully
    mongoose.disconnect().then(() => { // Close MongoDB connection after server is closed
      console.log('MongoDB disconnected.');
      console.log('Server closed.');
      process.exit(0); // Exit the process cleanly
    }).catch(err => {
        console.error('MongoDB disconnection error:', err)
        process.exit(1)
    })
  });
});