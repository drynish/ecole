import express from 'express';
import Todo from '../models/Todo';

const router = express.Router();

// Route pour créer un nouveau Todo
router.post('/todos', async (req, res) => {
  try {
    console.log(req.body)
    const todo = new Todo(req.body);
    await todo.save();
    res.status(201).json(todo); // Use 201 Created and send JSON
  } catch (err: unknown) {
    if (err instanceof Error) {
      res.status(400).json({ error: err.message }); // 400 Bad Request for validation errors
    } else {
      console.error("An unknown error occurred:", err); // Log the unknown error for debugging
      res.status(500).json({ error: 'An unknown error occurred' });
    }
  }
});

// Route pour récupérer tous les Todos
router.get('/todos', async (req, res) => {
  try {
    const todos = await Todo.find();
    res.status(200).json(todos); // Send JSON
  } catch (err: unknown) {
    if (err instanceof Error) {
      res.status(500).json({ error: err.message });
    } else {
      console.error("An unknown error occurred:", err); // Log the unknown error
      res.status(500).json({ error: 'An unknown error occurred' });
    }
  }
});

export default router;