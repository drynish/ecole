import mongoose, { Document, Schema } from 'mongoose';

export interface ITodo extends Document {
  title: string;
  description?: string;
  completed: boolean; // Add a completed field
  createdAt: Date;
  updatedAt: Date; // Add an updatedAt field
}

const TodoSchema = new Schema<ITodo>(
  {
    title: { type: String, required: true, trim: true },
    description: { type: String, trim: true }, 
    completed: { type: Boolean, default: false }, 
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date }, 
  },
  { timestamps: true } 
);

TodoSchema.pre('save', function (next) {
  this.updatedAt = new Date();
  next();
});

const Todo = mongoose.model<ITodo>('Todo', TodoSchema);
export default Todo;