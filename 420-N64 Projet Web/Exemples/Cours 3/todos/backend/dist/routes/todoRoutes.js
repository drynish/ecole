"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Todo_1 = __importDefault(require("../models/Todo"));
const router = express_1.default.Router();
// Route pour créer un nouveau Todo
router.post('/todos', async (req, res) => {
    try {
        console.log(req.body);
        const todo = new Todo_1.default(req.body);
        await todo.save();
        res.status(201).json(todo); // Use 201 Created and send JSON
    }
    catch (err) {
        if (err instanceof Error) {
            res.status(400).json({ error: err.message }); // 400 Bad Request for validation errors
        }
        else {
            console.error("An unknown error occurred:", err); // Log the unknown error for debugging
            res.status(500).json({ error: 'An unknown error occurred' });
        }
    }
});
// Route pour récupérer tous les Todos
router.get('/todos', async (req, res) => {
    try {
        const todos = await Todo_1.default.find();
        res.status(200).json(todos); // Send JSON
    }
    catch (err) {
        if (err instanceof Error) {
            res.status(500).json({ error: err.message });
        }
        else {
            console.error("An unknown error occurred:", err); // Log the unknown error
            res.status(500).json({ error: 'An unknown error occurred' });
        }
    }
});
/*
// Route pour mettre à jour un Todo
todoRoutes.put('/todos/:id', async (req, res) => {
  try {
    const todo = await Todo.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    if (!todo) {
      return res.status(404).json({ error: 'Todo not found' }); // Send JSON error
    }
    res.json(todo); // Send JSON
  } catch (err: unknown) {
    if (err instanceof Error) {
      res.status(400).json({ error: err.message }); // 400 for validation errors
    } else {
      console.error("An unknown error occurred:", err); // Log the unknown error
      res.status(500).json({ error: 'An unknown error occurred' });
    }
  }
});

// Route pour supprimer un Todo
todoRoutes.delete('/todos/:id', async (req, res) => {
  try {
    const todo = await Todo.findByIdAndDelete(req.params.id);
    if (!todo) {
      return res.status(404).json({ error: 'Todo not found' }); // Send JSON error
    }
    res.status(204).end(); // 204 No Content for successful delete
  } catch (err: unknown) {
    if (err instanceof Error) {
      res.status(500).json({ error: err.message });
    } else {
      console.error("An unknown error occurred:", err); // Log the unknown error
      res.status(500).json({ error: 'An unknown error occurred' });
    }
  }
});
*/
exports.default = router;
