"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = __importDefault(require("mongoose"));
const dotenv_1 = __importDefault(require("dotenv")); // Import dotenv
const todoRoutes_1 = __importDefault(require("./routes/todoRoutes"));
// Load environment variables from .env file
dotenv_1.default.config();
const app = (0, express_1.default)();
// Middleware
app.use((0, cors_1.default)()); // Enable CORS for all origins (for development).  Restrict in production!
app.use(body_parser_1.default.json());
// Routes
app.use('/api', todoRoutes_1.default);
// MongoDB Connection
const mongoURI = process.env.MONGODB_URI || 'mongodb://localhost:27017/mean-stack'; // Use env variable or default
mongoose_1.default.connect(mongoURI).then(() => {
    console.log('Connected to MongoDB');
}).catch((err) => {
    console.error('MongoDB connection error:', err);
    process.exit(1); // Exit the process if the connection fails
});
const PORT = process.env.PORT || 3000; // Use environment variable for port
const server = app.listen(PORT, () => {
    console.log(`Server running on http://localhost:${PORT}`);
});
// Graceful Shutdown (Important!)
process.on('SIGINT', () => {
    console.log('Shutting down server...');
    server.close(() => {
        mongoose_1.default.disconnect().then(() => {
            console.log('MongoDB disconnected.');
            console.log('Server closed.');
            process.exit(0); // Exit the process cleanly
        }).catch(err => {
            console.error('MongoDB disconnection error:', err);
            process.exit(1);
        });
    });
});
