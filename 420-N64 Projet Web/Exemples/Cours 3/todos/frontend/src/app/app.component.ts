import { Component } from '@angular/core';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoAddComponent } from "./todo-add/todo-add.component";

@Component({
  standalone: true,
  selector: 'app-root',
  imports: [TodoListComponent, TodoAddComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent {
  title = 'Blog';
}
