import { Component } from '@angular/core';
import { Todo } from '../../model/todo';
import { TodoService } from '../todo.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-todo-add',
  imports: [ FormsModule ],
  templateUrl: './todo-add.component.html',
  styleUrl: './todo-add.component.css'
})
export class TodoAddComponent {

  todo: Todo = {
    title: '',
    description: '',
  };
  
  constructor(private todoService: TodoService) { }

  addTodo(): void {
    this.todoService.addTodo(this.todo);
    this.todo.title = '';
    this.todo.description = '';  
  }
}
