import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { FormsModule } from '@angular/forms';
import { Todo } from '../../model/todo';

@Component({
  standalone: true,
  selector: 'app-todo-list',
  imports: [CommonModule, FormsModule],
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.css',
})
export class TodoListComponent implements OnInit {

  todos: Todo[] = [];

  
  constructor(private todoService: TodoService) {};

  ngOnInit(): void {
    this.todoService.todos$.subscribe((todos) => {
      this.todos = todos;  // Met à jour la liste des todos lorsque l'Observable émet une nouvelle valeur
    });
    this.todoService.getTodos();  // Récupère les todos initiaux
  }
  
}
