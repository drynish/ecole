import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../model/todo';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TodoService {
  private todosSubject = new BehaviorSubject<Todo[]>([]);
  private apiUrl = 'http://localhost:3000/api/todos';

  todos$ = this.todosSubject.asObservable();

  constructor(private http: HttpClient) {}

  getTodos() {
    this.http.get<Todo[]>(this.apiUrl).subscribe((todos) => {
      this.todosSubject.next(todos);  // Mets à jour la liste des todos
    });
  }


  addTodo(todo: Todo) {
    this.http.post<Todo>(this.apiUrl, todo).subscribe((newTodo) => {
      this.todosSubject.next([...this.todosSubject.value, newTodo]); // Ajoute le nouveau todo et met à jour
    });
  }

}
