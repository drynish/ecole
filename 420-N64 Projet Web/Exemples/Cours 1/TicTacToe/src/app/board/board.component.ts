import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-board',
  standalone: true,
  imports: [ CommonModule ],
  templateUrl: './board.component.html',
  styleUrl: './board.component.css'
})
export class BoardComponent {
  board: string[][] = [
    ['', '', ''],
    ['', '', ''],
    ['', '', '']
  ];
  currentPlayer: string = 'X';

  makeMove(row: number, col: number) {
    if (this.board[row][col] === '') {
      this.board[row][col] = this.currentPlayer;
      this.currentPlayer = this.currentPlayer == 'X' ? 'O' : 'X';
      this.checkWinner(); // Vérifier s'il y a un gagnant après chaque coup
    }
  }

  checkWinner() {
    const lines = [
      [0, 1, 2], // Lignes
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6], // Colonnes
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8], // Diagonales
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (this.board[Math.floor(a/3)][a%3] &&
          this.board[Math.floor(a/3)][a%3] === this.board[Math.floor(b/3)][b%3] &&
          this.board[Math.floor(a/3)][a%3] === this.board[Math.floor(c/3)][c%3]) {
        alert('Le joueur ' + this.board[Math.floor(a/3)][a%3] + ' a gagné !');
        this.resetBoard(); // Réinitialiser le plateau après la victoire
        return;
      }
    }
  }

  resetBoard(){
      this.board = [
          ['', '', ''],
          ['', '', ''],
          ['', '', '']
        ];
        this.currentPlayer = 'X';
  }
}
