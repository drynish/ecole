import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WishItem } from '../shared/models/wishItem';

const filters = [
  (item: WishItem) => item,
  (item: WishItem) => !item.isCompleted,
  (item: WishItem) => item.isCompleted
];

@Component({
  selector: 'app-root',
  imports: [CommonModule, FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  items: WishItem[] = [
    new WishItem("Sortir les poubelles sans avoir froid"),
    new WishItem("Bien chanter", false),
    new WishItem("La vaiselle est faîte!", true),
    new WishItem("patate")
  ]
  title = 'wishlist';
  newWishText = "";
  listFilter: any = "0";

  get visibleItems(): WishItem[] {
    return this.items.filter(filters[this.listFilter])
  };

  addNewWish() {
    this.items.push(new WishItem(this.newWishText));
    this.newWishText = '';
    // todo: add wish to items array
    // clear the textbox
  }

  toggleItem(item: WishItem) {
    item.isCompleted = !item.isCompleted
    console.log(item);
  }
}
