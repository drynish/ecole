const express = require('express');
const path = require('path');

const app = express();
const PORT = 3000;

// Servir les fichiers Angular compilés
app.use(express.static(path.join(__dirname, '../frontend/dist')));

// Exemple d'API backend
app.get('/api/hello', (req, res) => {
  res.json({ message: 'Hello from Express backend!' });
});

// Gérer les routes Angular SPA
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../frontend/dist', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

// Il faudra compiler le frontend en utilisant la commande suivante: ng build --prod pour créer les fichiers nécessaires pour être en mesure de le rouler statiquement.