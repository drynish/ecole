"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken")); // Import de jsonwebtoken pour générer un token
const dotenv_1 = __importDefault(require("dotenv"));
const app = (0, express_1.default)();
const PORT = 3000;
// Middleware pour parser les JSON
app.use(express_1.default.json());
// Route de base
app.get('/', (req, res) => {
    res.send('Hello, Express avec TypeScript et JWT!');
});
dotenv_1.default.config();
const JWT_SECRET = process.env.JWT_SECRET || 'votre_clé_secrète';
// Variables statiques pour login et mot de passe
const username = 'admin';
const password = 'password';
// Route de connexion
// Route de connexion
app.post('/login', (req, res) => {
    const { username: providedUsername, password: providedPassword } = req.body;
    // Vérification des identifiants
    if (providedUsername === username && providedPassword === password) {
        // Générer un token JWT
        const token = jsonwebtoken_1.default.sign({ username: providedUsername }, JWT_SECRET, { expiresIn: '1h' });
        // Répondre avec le token
        return res.json({ message: 'Connexion réussie', token });
    }
    else {
        // Si les identifiants sont incorrects
        return res.status(401).json({ message: 'Identifiants invalides' });
    }
});
// Lancement du serveur
app.listen(PORT, () => {
    console.log(`Serveur démarré : http://localhost:${PORT}`);
});
