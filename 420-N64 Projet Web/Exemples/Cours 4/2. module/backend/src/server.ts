import express, { Request, Response } from 'express';
import jwt from 'jsonwebtoken'; // Import de jsonwebtoken pour générer un token
import dotenv from 'dotenv';
import cors from 'cors';

const app = express();
const PORT = 3000;

const corsOptions = {
  origin: 'http://localhost:4200', // Remplacer par l'URL de ton front-end Angular
  methods: ['GET', 'POST', 'PUT', 'DELETE'], // Méthodes HTTP autorisées
  allowedHeaders: ['Content-Type', 'Authorization'], // En-têtes autorisés
};

// Activer CORS pour toutes les origines
app.use(cors(corsOptions));
app.options('*', cors(corsOptions))

// Middleware pour parser les JSON
app.use(express.json());

// Route de base
app.get('/', (req: Request, res: Response) => {
  res.send('Hello, Express avec TypeScript et JWT!');
});

dotenv.config()
const JWT_SECRET = process.env.JWT_SECRET || 'votre_clé_secrète';

// Variables statiques pour login et mot de passe
const username = 'admin';
const password = 'password';

// Route de connexion
app.post('/login', async (req: Request, res: Response):Promise<any> => {
  const { username: providedUsername, password: providedPassword } = req.body;

  // Vérification des identifiants
  if (providedUsername === username && providedPassword === password) {
    // Générer un token JWT
    const token = jwt.sign({ username: providedUsername }, JWT_SECRET, { expiresIn: '1h' });

    // Répondre avec le token
    return res.json({ message: 'Connexion réussie', token });
  } else {
    // Si les identifiants sont incorrects
    return res.status(401).json({ message: 'Identifiants invalides' });
  }
});

// Lancement du serveur
app.listen(PORT, () => {
  console.log(`Serveur démarré : http://localhost:${PORT}`);
});