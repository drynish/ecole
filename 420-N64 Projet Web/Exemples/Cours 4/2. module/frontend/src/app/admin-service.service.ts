import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

    private apiUrl = '/api'; // Remplacez par votre URL d'API
  
    constructor(private http: HttpClient) {}
  
    // Méthode pour envoyer une requête de connexion
    login(username: string, password: string): Observable<any> {
      const payload = { username, password };
      const headers = new HttpHeaders().set('Content-Type', 'application/json');
      return this.http.post(`${this.apiUrl}/login`, payload, {headers});
    }
  
}
