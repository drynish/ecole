import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: 'admin',
        loadComponent: () => {
            return import('./admin/login/login.component').then((m) => m.LoginComponent)
        },
      },
];
