import { Component } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminServiceService } from '../../admin-service.service';

@Component({
  standalone: true,
  selector: 'app-login',
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent{

  constructor(private adminService: AdminServiceService) {};

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  get username() {
    return this.loginForm.get('username')!;
  }

  get password() {
    return this.loginForm.get('password')!;
  }


  onSubmit() {
    if (this.loginForm.valid) {
      const { username, password } = this.loginForm.value;

      const usernameStr = username ?? ''; 
      const passwordStr = password ?? '';

      this.adminService.login(usernameStr, passwordStr).subscribe(
        (response) => {
          // Ici, tu peux gérer la réponse, par exemple en stockant le token JWT
          console.log('Connexion réussie', response);
        }), 
        (error: any) => {
          console.log('Erreur de connexion', error);
        }
      //console.log('Formulaire envoyé :', this.loginForm.value);
    }
  }
}
