# Atelier

Travail individuel. Compte pour 10% de votre session.ß

Date de remise: Lundi 13 février 12h50

Remise sur moodle

Vous devez faire un système de blogue très simpliste.

Un article contient:
 - Un titre
 - Un message texte (pas de HTML)
 - Une date de création (il faut afficher: créé il y a X jours, par exemple)

Un utilisateur anonyme peut:
 - Lire tous les messages
 - Se connecter et se créer un compte

Un utilisateur connecté peut:
 - Se déconnecter
 - CRUD de ses messages à lui

Autres détail:
 - Vous devez utiliser un framework CSS ou un thème CSS
 - Vous pouvez seulement utiliser les paquets npm vus dans le cours
 
Correction:
 - Connexion - /1
 - Création de compte - /2
 - Déconnexion - /1
 - Lire tous les messages - /1
 - CRUD de ses messages - /3
 - Sécurité du CRUD - /2

Pour avoir tous vos points, votre code doit:
 - Respecter les normes de programmations Angular et JavaScript
 - Bien fonctionner sans effet de bord (pas de bugs)