# 420-N64 - Projet Web

[![CC BY-NC-ND 4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-nd.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr)

## Documents

  * [Plan de cours](N64%20-%20Plan%20de%20cours%202025.pdf)


## Notes de cours

 * [MEAN](notesdecours/mean.md)
 * [TypeScript](notesdecours/typescript.md)
 * [Angular](notesdecours/angular/angular.md)
   * [Composants](notesdecours/angular/component.md)
   * [Formulaires](notesdecours/angular/forms.md)
   * [Services](notesdecours/angular/service.md)
   * [Modules](notesdecours/angular/module.md)
   * [Guarde](notesdecours/angular/guard.md)
   * [Proxy](notesdecours/angular/proxy.md)
 * [Express](notesdecours/express/express.md)
   * [Contrôleur](notesdecours/express/controller.md)
   * [Routes](notesdecours/express/routes.md)
   * [Médiateurs](notesdecours/express/middleware.md)
   * [Communication Angular-Express](notesdecours/express/communication.md)
   * [PostgreSQL](notesdecours/express/postgresql.md)
   * [MongoDB](notesdecours/express/mongodb.md)
   * [Déploiement](notesdecours/express/deploy.md)
 * Sécurité
   * [XSS et CSRF](notesdecours/securite/xss_csrf.md)
   * [Mot de passe](notesdecours/securite/password.md)
   * [JWT](notesdecours/securite/jwt.md)