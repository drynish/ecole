# Services

Les services rassemblent des fonctionnalités qui n'ont pas d'interfaces dédiés. Principalement, on va créer un service par table dans notre base de données pour communiquer avec l'API. On pourrait aussi avoir un service qui gère les journaux d'erreurs ou un service qui gère les messages d'événements à afficher dans le logiciel. Dans ce dernier cas un service servirait à rassembler les messages et un composant servira à les afficher.

## Service simple

Pour créer un service exécutez la commande suivante:

```
ng g s nom-du-service
```

Par défaut il crée le service à la racine du module app, on pourrait créer manuellement un dossier "services" et changer le nom du service pour "services/nom-du-service" dans la commande précédente. Ensuite pour utiliser le service il faut utiliser l'injection de dépendances:

```
constructor(private nomDuService: NomDuServiceService) {}
```

On ajoute donc le service en paramètre dans le constructeur (du composant ou d'un autre service) et TypeScript va créer la propriété privé qui contiendra le service.

## Marque substitutive (placeholder)

Nous n'avons pas encore de serveur, ce qui nous empêche de faire nos appels d'API. Par contre, nous pouvons préparer nos services d'appels aux API afin de limiter les modifications lorsque ce sera prêt. Nos fonctions qui consulterons l'API vont retourner le type Observable<T> (il faut le import de rxjs). Nous devons donc retourner le même type sans appel d'API, nous aurons donc besoin de la fonction "of" de rxjs qui retourne le même type. of nous servira de simulacre (mock) d'appel HTTP. En résumé, nous avons besoin de l'import:

```
import { Observable, of } from "rxjs";
```

Et notre fonction getAll ressemblera à:

```
getAll(): Observable<User[]> {
  return of(this.users);
}
```

Le composant qui utilisera ce service recevra un objet observable, qui est similaire à une promesse JavaScript. Exemple, pour voir la liste des users:

```
ngOnInit(): void {
  this.usersService.getAll().subscribe(users => {
    this.users = users;
  },
  error => {
    // Gestion de l'erreur
    this.users = [];
    this.error = error;
  });
}
```

La fonction subscribe (fonction d'un Observable) a 2 paramètres:

 1. Arrow function qui reçoit la réponse JSON du serveur en paramètre
 2. Arrow function qui reçoit l'erreur du serveur en paramètre