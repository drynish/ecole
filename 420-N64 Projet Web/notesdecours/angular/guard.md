# Garde Angular

Un garde Angular est un principe qui permet de sécuriser des routes. Premièrement, on va créer un service d'autorisation, ce service sera utilisé par le garde et vos vues pour afficher/cacher des éléments HTML. Dans ce service je vais mettre la fonction suivante:

```ts
isLoggedIn(): boolean {
  let user = localStorage.getItem('user');
  return user != null;
}
```

Notez que dans ce cas le localStorage doit être remplis à la connexion utilisateur.

Maintenant on va créer le garde avec la commande suivante:

```
ng g guard auth
```

La ligne de commande va vous demander quelles interfaces vous voulez implémenter. Chaque interface active une fonctionnalité différente:

 * CanActivate: Détermine si la route est accessible, ne sera pas appelé si on passe de cette page vers une page enfant, mais sera utilisé si on accède directement à l'enfant
 * CanActivateChild: Détermine si les enfants de cette route sont accessibles, ne sera pas appelé pour cette route mais sera appelé pour tous les enfants
 * CanDeactivate: Peut être utilisé pour empêcher un utilisateur de quitter une page, utile lorsqu'un formulaire est modifié mais non sauvegardé
 * CanLoad: Très similaire à CanActivate, mais ne charge pas le contenu si l'accès est refusé, utile pour le module Admin par exemple.

Une fois le fichier créé, on va ajouter le constructeur afin d'ajouter l'injection de dépendance pour notre service précédemment créé.

Appelez le service afin de savoir si l'utilisateur peut activer cette route ou non.

Finalement, on ajoute le garde à notre router:

```ts
const routes: Routes = [
  { path: 'create', component: CreateComponent, canActivate: [AuthGuard] }
];
```

Si vous testez, vous remarquerez que votre lien vers cette page ne fonctionne plus. Si on veut rediriger vers une autre page, au lieu de 'return false' dans notre garde, on peut retourner un chemin comme ceci (vous devez injecter le service Router):

```ts
return this.router.parseUrl('/login');
```