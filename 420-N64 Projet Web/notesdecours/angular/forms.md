# Formulaire

Avec la liaison à deux sens du dernier chapitre on peut facilement créer un formulaire simple. Angular gère bien les vérifications HTML comme required et min/max. Par contre, cette pratique est une vielle technique qui provient de feu AngularJS appelé Formulaire mené par modèle (Template driven form). La nouvelle technique, Formulaire réactif (Reactive Forms), est plus complexe, mais permet de créer des formulaires par programmation, de bien tester notre composante et de mettre plus de logique vers le fichier TS.

![Angular forms](../../images/angular-forms.png)

Source de l'image: https://stackoverflow.com/a/51435178

Pour commencer, il faut importer le module de forms:

```
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
```

Ensuite dans notre classe il faut déclarer notre formulaire:

```
loginForm: FormGroup;
```

Et il faut recevoir le service de formulaires réactif dans notre classe, on modifie donc le constructeur afin de bénificier de l'injection de dépendence:

```
constructor(private formBuilder: FormBuilder) {}
```

Maintenant, dans le ngOnInit on peut initialiser notre formulaire, par exemple:

```
ngOnInit(): void {
  this.loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
}
```

Notre formulaire contient donc 2 champs: username et password. Par défaut, les deux ont une valeur vide (première valeur dans le tableau) et les deux sont requis. On peut mettre plusieurs validateurs en donnant un tableau de validateurs en 2e élément de tableau, on peut même créer nos validateurs.

Notre formulaire est prêt à être utilisé, il faut maintenant aller le lier au HTML (j'ai enlevé beaucoup de code pour garder l'essentiel):

```
<form [formGroup]="loginForm" (ngSubmit)="onSubmit()">
  <input formControlName="username"/>
  <input formControlName="password" type="password"/>
  <button type="submit">Connexion</button>
</form>
```

Les points importants:

  1. Dans le formulaire on identifie le [formGroup] qui contient le nom de notre propriété du même type
  2. Pour chaque contrôle, on ajoute le 'formControlName' qui correspond au champ présent dans notre formGroup
  3. Petit bonus, j'ai ajouté le code nécessaire à la soumission d'un formulaire. On aurais aussi pu mettre un onClic sur le bouton

Maintenant quelques lignes de codes intéressants pour le onSubmit:

```
this.loginForm.invalid; // Booléen, vrai s'il y a une erreur
this.loginForm.getRawValue(); // Retourne un objet anonyme contenant les valeurs des champs
this.loginForm.controls.username.errors.required; // Retourne vrai si username ne respecte pas la validation required
```

La première ligne de code est utile pour enregistrer seulement s'il n'y a pas d'erreur.
La deuxième ligne de code permet de récupérer vos données. Si vous avez un modèle avec exactement les mêmes propriétés que votre formulaire, vous pouvez typer la variable qui recevra les valeurs.
La troisième ligne de code peut être utilisé dans le composant ou dans la vue. Dans le composant on pourra récupérer une seule erreur et afficher le message correspondant tandis que dans la vue on pourrait afficher tous les messages possibles avec des ngIf.

## Bootstrap
Si vous utilisez Bootstrap, vous devez faire quelques modifications pour que la validation fonctionne bien. Commencez par enlever la référence vers le bootstrap.css du fichier angular.json. Ensuite dans le fichier src/styles.scss (qui sera inclus dans tous les modules/composants), inscrivez ceci:

```
@import "~bootstrap/scss/bootstrap";
  .ng-touched.ng-invalid {
  @extend .is-invalid
}
.ng-touched.ng-valid {
  @extend .is-valid
}
```

Ça active le style d'erreurs Bootstrap avec les classes de Angular. Exemple de HTML du formulaire:

```
<form [formGroup]="loginForm" (ngSubmit)="login()">
  <div>
    <label for="username">Nom d'utilisateur</label>
    <input formControlName="username" id="username" class="form-control"/>
    <div class="invalid-feedback">Le nom d'utilisateur est obligatoire</div>
  </div>
  <button type="submit" class="btn btn-primary">Se connecter</button>
</form>
```

Notez que la classe "invalid-feedback' apparait seulement si l'élément précédent est invalide. La fonction 'login()':

```
login():void {
  if (this.loginForm.valid) {
    // Appeler service
  }
  else {
    Object.keys(this.loginForm.controls).forEach(field => {
    const control = this.loginForm.get(field);
    control.markAsTouched({onlySelf: true});
  });
}
```
Le else est là afin de valider manuellement les champs. Sans ça, si un utilisateur clique sur le bouton sans donner le focus aux contrôles, les messages d'erreurs ne s'affichent pas.
