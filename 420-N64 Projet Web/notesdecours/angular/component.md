# Composant

Si vous créez un composant, vous allez voir 3 fichiers (4 si les tests sont activés). Chaque composant a son propre dossier. Par exemple, pour le composant login, vous trouvez dans le dossier login:

 * login.component.html - La vue, gère l'affichage seulement
 * login.component.scss - Votre feuille de style lié à la vue. Angular va modifier toutes les balises de la vue et tous les sélecteurs CSS, il ajoute un attribut HTML, par exemple, '_ngcontent-gvp-c45'.
 * login.component.ts - Le contrôleur, fait le lien entre les fichiers du composant et avec le reste du logiciel
 * login.component.spec.ts - Fichier de test unitaire

Tout ce qui est publique dans la classe du contrôleur est disponible dans la vue (propriété et fonction). Deux fonctions importantes sont déjà dans la classe:

  1. constructor: Le constructeur est appelé à la création de la composante, on va le laisser vide presque tout le temps. En paramètre, on va lui donner les services nécessaire au fonctionnement de notre composante (ex: constructor (private formBuilder: FormBuilder) {}). L'injection de dépendance va donner le bon service à notre classe qui va le garder en propriété privée.
  2. ngOnInit: Cette fonction est appelé lorsque le gabarit (template) est généré et prêt à être manipulé. On va y inscrire le code à exécuter au démarrage du composant.

## Afficher une valeur

Pour afficher une valeur dans la vue, il faut la déclarer dans le contrôleur puis utiliser les moustaches dans la vue:

```
# Fichier ts
export class LoginComponent implements OnInit {
  count: number = 0;
}
# Fichier HTML
{{ count }}
```

## Fonction et événement

Pour utiliser une fonction, il faut lier une fonction du contrôleur à un événement DOM dans le HTML. Par exemple, on va ajouter un onClick sur un bouton qui incrémente le compte:

```
# Fichier TS
export class LoginComponent implements OnInit {
  count: number = 0;
  incrementer(): void {
    this.count++;
  }
}

# Fichier HTML
{{ count }} <br/>
<button (click)="incrementer()">Incrémenter</button>
```

Les parenthèses sont importantes autour du 'click' dans le HTML, c'est ce qui indique à Angular qu'on va lier un événement.

## Manipulation d'attributs

On peut aussi manipuler les attributs HTML, par exemple si on veut ajouter une classe CSS à une balise HTML selon certaines conditions. Le cas classique est d'afficher un message d'erreur seulement lorsqu'il y en a un:

```
<div class="alert alert-danger" [ngClass]="{'d-none': error == ''}">{{error}}</div>
```

ngClass permet d'ajouter des classes selon une condition. Il faut lui donner un objet anonyme, la 'clé' est le nom de la propriété à ajouter et sa valeur est la condition. C'est 'weird' mais ça marche. Dans cet exemple on ajoute la classe 'd-none' si 'error' est vide. 'd-none' est une classe bootstrap qui ajoute le CSS 'display: none'.

## Conditions et boucles

Encore mieux que mettre la classe 'd-none', on pourrait enlever complètement la balise lorsqu'elle n'est pas nécessaire. C'est possible avec l'instruction '*ngIf' (l'étoile est importante, sans elle il faudrait 'wrapper' notre balise par une balise ng-template) :

```
<div class="alert alert-danger" *ngIf="error">{{error}}</div>
```

'error' est une string, une string vide vaut false. Si 'error' est vide la balise ne sera pas affiché et le contenu de la balise ne sera pas interprété par Angular. Si on a plusieurs messages d'erreurs, on peut utiliser l'instruction '*ngFor':

```
<div class="alert alert-danger" *ngIf="errors">
  <ul>
    <li *ngFor="let error of errors">
      {{error}}
    </li>
  </ul>
</div>
```

La balise contenant le '*ngFor' sera copié pour chaque élément du tableau. Si on a 3 messages d'erreurs, il y aura 3 li. Notez que j'ai quand même mis le ngIf sur le div principal afin de ne pas afficher le alert s'il n'y a pas d'erreur.

Pour plus de détails, ou pour voir les switch, voir cette page: https://angular.io/guide/structural-directives.

## Liaison à deux-sens (2 way binding)

C'est possible de lier une propriété du contrôleur à un champ modifiable par l'utilisateur. En plus d'afficher la valeur par défaut, l'utilisateur pourra modifier directement cette propriété:

```
<input [(ngModel)]="username" />
```

## Sous-composants

Vous pouvez inclure des composants dans d'autres composants. Vous pouvez déjà le voir dans la vue du composant racine 'app/index.html'. Pour inclure un composant dans un autre, il faut l'inclure dans la vue en écrivant son nom complet, incluant des modules. Par exemple, pour afficher le composant login qui se trouve dans le module par défaut, il faut mettre la balise suivante dans la vue racine:

```
<app-login></app-login>
```

Parfois on va aussi vouloir transférer des données entre le composant parent et le composant enfant.

### Parent → Enfant

On commence par configurer l'enfant, on ajoute l'import "Input" de "@angular/core" (devrait déjà import Component). Ensuite on ajoute la propriété à recevoir:

```
@Input() Nombre : Number;
```

Et dans le HTML on va l'afficher avec les moustaches:

```
{{Nombre}}
```

Maintenant on configure le parent. On va ajouter la propriété dans la classe:

```
NombreAEnvoyer : Number = 42;
```

Et dans son HTML on ajoute un attribut qui va lier Nombre de l'enfant à NombreAEnvoyer du parent:

```
<app-test-enfant [Nb]="NombreAEnvoyer"></app-test-enfant>
```

L'enfant va donc afficher 42. A noter que si le parent modifie NombreAEnvoyer, Nombre de l'enfant sera aussi modifié. Par contre l'inverse n'est pas vrai. Modifier Nombre dans l'enfant ne fera rien pour le parent. Si vous voulez effectuer une action dans l'enfant lorsque le Input change, utilisez la méthode ngOnChanges. Cette méthode a besoin des imports "OnChanges" et "SimpleChanges" de "@angular/core". Votre classe doit aussi implémenter l'interface "OnChanges". La méthode va ressembler à:

```
ngOnChanges(changes: SimpleChanges): void {}
```

changes est un objet qui contient une propriété par input modifié. Donc si vous avez deux Inputs différents la méthode sera appelé lorsque un des deux est modifié. Il faut donc regarder dans les propriétés membres de l'objet quel Input a été modifié. Exemple (en JSON) de changes:

```
{"Nb":{"currentValue":0,"firstChange":true},"Nb2":{"currentValue":2,"firstChange":true}}
```

### Enfant → Parent

Maintenant si on veut faire l'inverse, notre enfant veut envoyer des données vers le parent. Par exemple l'enfant est un formulaire de modification et on a sauvegardé, le parent doit fermer cet enfant. On va quand même utiliser un exemple simple avec des nombres.

On commence encore par l'enfant, on doit ajouter les imports "Output" et "EventEmitter" de "@angular/core". Maintenant on ajoute la propriété suivante:

```
@Output() totalEvent = new EventEmitter<number>();
```

On a créé un événement appelé totalEvent qui servira en envoyer un total à notre parent. Cet événement va envoyer un nombre. Maintenant on crée d'autres variables et une fonction qu'on pourra lier à un bouton:

```
nb1: number = 1;
nb2: number = 2;
calculerTotal(): void {
  let total = this.nb1 + this.nb2;
  this.totalEvent.emit(total);
}
```

La ligne importante est le "emit" qui déclenche l'événement. Vous savez déjà comment faire un onClick sur un bouton, je passe cette partie ;)

Maintenant on prépare le parent. On va commencer par créer la fonction qui sera appelé lorsque l'enfant va déclencher son événement.

```
totaux: number[] = [];
  addTotal(nouveauTotal: number) {
  this.totaux.push(nouveauTotal);
}
```

Et maintenant pour afficher le tout on va mettre dans la vue du parent:

```
<app-test-enfant (totalEvent)="addTotal($event)"></app-test-enfant>
```

Et voila, votre parent va recevoir les événements de l'enfant. Remarquez bien dans l'exemple de code précédent que certaines parties viennent de l'enfant et d'autres du parent. Si vous voulez passer plusieurs paramètres avec Output, c'est la même procédure mais vous devez envoyer/recevoir des objets.

Vous voulez recevoir et envoyer des données entre un parent et un enfant? Vous pouvez combiner les deux méthodes sans problèmes.

Source pour presque tout dans cette page: https://angular.io/guide/inputs-outputs