# Déploiement

## Frontend (Angular)

Sur votre serveur, installez nginx ou apache2.

Sur votre ordinateur, dans votre dossier WWW, roulez la commande: `npm run build`.

Copiez le contenu du dossier créé (`/WWW/dist/nomduprojet`) dans le dossier web du serveur (`/var/www/html`).

## Backend (Express)

Installez node.js et nodemon en global (`npm install -g nodemon`).

Copiez votre API au complet sur le serveur.

Démarez votre API avec nodemon.

Dans nginx ou apache, configurez un proxy de `/api` vers le port express (3000).