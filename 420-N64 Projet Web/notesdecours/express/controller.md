# Contrôleur

Pour l'instant notre code est entièrement dans le fichier index.ts. À long terme ça va créer un problème de complexité, on va donc enlever la logique de traitement des requêtes du index.ts et la mettre dans des contrôleurs.

Un contrôleur peut être un objet, ou simplement un fichier qui contient des fonctions. Dans les deux cas il faut faire la même chose.

Premièrement on crée notre fonction qui va prendre la requête et renvoyer la réponse:

```js
function getAllUsers(req, res) {
  res.json([]);
}
```

Et à la fin du fichier il faut exporter notre classe ou nos fonctions:

```js
export{getAllUsers};
```

Notez qu'il faudra importer des modules comme celui ci:

```js
import { Response } from 'express';
```

Maintenant on importe notre contrôleur dans notre index.ts et on change la route:

```js
app.get('/users', getAllUsers);
```