# Express

Express est un serveur simple Node.js. C'est lui qui va recevoir les appels d'API et retourner le JSON sur le réseau, ainsi que communiquer
avec la base de données.

On va modifier notre projet dans Webstorm, on va déplacer tout notre code Angular actuel dans un dossier "client" et nous allons créer
un dossier "server". On pourrait aussi utiliser les termes "WWW" et "API".

Dans votre dossier server on va initialiser npm et installer les dépendances suivantes:
 - body-parser : Sert à lire le body dans les appels API (pour les POST)
 - cors : Ce nom qui fait faire des cauchemars, on doit l'installer...
 - express : Notre serveur!
 - express-validator : Pour faire de la validation facilement (required, minLength, isEmail, etc.), même si on valide côté client il faut valider côté serveur!
 - mongoose : Le SQL ça fait 1990, on touche pas à ça, surtout que Mongo ne supporte pas SQL, c'est du NoSQL ;)
 - bcryptjs : Pour encrypter les mots de passes
 - jsonwebtoken : Pour créer et valider des JWT facilement
 - cookie-parser : Lire et créer des cookies, cookie monster approves

Pour la BD, ajoutez la dépendance correspondante:
 - mongoose: Pour MongoDB
 - pg-promise: Pour PostgreSQL
 - nano: Pour CouchDB
 - Pour les autres, consultez la page https://expressjs.com/en/guide/database-integration.html

Et les dépendances de dev:
 - @types/body-parser: Activer l'autocomplétion pour cette dépendance
 - @types/cors: Idem
 - @types/express: Idem
 - @types/node: Idem
 - nodemon: Iä! Iä! Cthulu fhtagn! (restart le server s'il plante ou est modifié, installation globale (-g) sur un serveur)

## Configuration de base

Créez un fichier 'index.js'. Voici le fichier:

```js
import express = required('express');
const app = express();
app.get('/', function(req, res) {
  res.send('ok');
});
app.listen(3000, () => {
  console.log('listening to post 3000');
});
```

Les deux premières lignes importent et créent l'objet express qui contient le serveur. Le 'app.get' est le routage simple, on attends un appel GET sur la racine de l'API.
Les paramètres sont req (la requête) et res (la réponse).
On peut utiliser 'res' pour envoyer n'importe quel type JavaScript.
Finalement, le app.listen démarre le serveur sur le port 3000, le console.log est simplement pour s'assurer que le serveur est bien démarré.

## Codes HTTP

Par défaut Express retourne des codes 200 (OK, avec réponse).
Par contre, on va parfois avoir des erreurs ou aucune réponse, il faut donc changer le code HTTP de la réponse.
Au lieu de retourner une réponse avec:

```js
res.send('bla');
```

On va utiliser parfois:

```js
res.status(200).send('bla');
```

Voici la liste des codes HTTP avec leur signification (seulement les codes intéressants selon moi):

 - 1XX: Les codes 100 à 199 signalent qu'on a reçu la requête mais que la réponse n'est pas prête, pas utile dans notre cas
 - 2XX: Les codes 200 à 299 signalent que la requête a été acceptée
   - 200: OK, Requête accepté et retourne une réponse
   - 201: Created, Une nouvelle ressource a été créé, la ressource créé est habituellement retourné (surtout pour avoir l'ID)
   - 202: Accepted, Requête acceptée mais non traitée, pourrait même ne pas fonctionner
   - 204: No Content, Requête accepté mais ne retourne rien (sur un update par exemple)
   - 205: Reset Content, Signale au client de rafraichir la vue
 - 3XX: Les codes 300 à 399 signalent une redirection, je ne pense pas que ça va être utile dans notre cas
 - 4XX: Les codes 400 à 499 signalent une erreur du client, ces codes seront très utiles
   - 400: Bad Request, la route existe mais les données ne sont pas bien formés (il manque un champ, erreur de type, etc.)
   - 401: Unauthorized, connecte toi stp
   - 403: Forbidden, hack ton JWT parce que tu n'as pas le droit d'accéder à cette route
   - 404: Not Found, la route n'existe pas
   - 418: I'm a teapot, Le serveur refuse d'infuser du café avec une théière (ce n'est pas une blague, ce code veut vraiment dire ça)
 - 5XX: Les codes 500 à 599 signalent une erreur serveur, un service indisponible, une méthode 'Not implemented', etc.

Pour la liste complète, voir https://developer.mozilla.org/fr/docs/Web/HTTP/Status