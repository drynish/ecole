# Routes

On a sortie notre logique de notre index.ts pour la mettre dans des contrôleurs, c'est bien, mais nous avons tout de même plusieurs routes dans notre index.ts. Notre fichier index devrait seulement 'bootstrap' notre application. On va donc sortir nos routes dans d'autres fichiers. Je me suis créé un dossier 'routes' et à l'intérieur on va créer 2 fichiers:

```js
// usersRoutes.jf
let express = require('express');
let router = express.Router();
import { getAllUsers } from '../controllers/usersController';
router.get('/', getAllUsers);
modules.exports = router;
export {};

// routes.js
let express = require('express');
let router = express.Router();
let usersRoutes = require('usersRoutes');
router.use('/users', usersRoutes);
module.exports = router;
export {};
```

usersRoutes.js rassemble toutes les routes pour les users et le fichier routes.js va rassembler tous les fichiers de routes. Ces deux fichiers ont une structure similaire: Importations, Routes et Exportations. Avec cette configuration, le 'getAllUsers' sera appelé avec l'URL '/users'.

Finalement, on retourne dans le index.ts, on importe notre fichier routes.ts et on remplace les lignes de routes par:
```js
app.use('/', routes);
```

## Verbes HTTP

 - get, pour recevoir de l'information
 - post, pour créer une resource
 - put, pour modifier une resource
 - delete, pour supprimer une resource