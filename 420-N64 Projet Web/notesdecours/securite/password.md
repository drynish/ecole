# Mot de passe

Les mots de passent doivent être encryptés dans la base de données, comme dans tout bon système. Pour ce faire, on a besoin de "bcryptjs" (à installer avec npm). Ensuite on l'importe et on peut encrypter:

```js
import Bcrypt from 'bcryptjs';
// Encrypter
Bcrypt.hashSync(password, 10);
// Vérifier un mot de passe
Brypt.compareSync(password, passwordBD);
```

La fonction hash retourne le mot de passe encrypté avec un 'salt' aléatoire. Le 2e paramètre est optionnel (10 par défaut), c'est le nombre de tour d'encryption, chaque incrément de 1 double le temps d'encryptions.

La fonction compare retourne un booléen, vrai si le premier paramètre (mot de passe brute) une fois encrypté, est égal au deuxième paramètre (mot de passe encrypté).