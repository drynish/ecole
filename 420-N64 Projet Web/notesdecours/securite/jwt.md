# JWT

Le JWT (Json Web Token) permet de gérer les autorisations. Le JWT est conservé dans un cookie et contient les autorisations donnés par le serveur. Pour le gérer, on va installer "jsonwebtoken" et "cookie-parser" par npm.

Le JWT est une solution idéale lorsque vous avez plusieurs serveurs. Que l'utilisateur se connecte au serveur 1 ou 2, il pourra valider son JWT. Par contre, c'est plus complexe que la session qui est active seulement sur un serveur.

Il faut activer le "cookie-parser" dans votre index.js:

```js
import cookieParser = require("cookie-parser");
app.use(cookieParser());
```

J'ai mis le 'app.use' de cookies juste avant le 'app.use' de cors.

Pour générer le JWT (après une connexion réussie), il suffit d'appeler la fonction de "jsonwebtoken" et de créer le cookie:

```js
import * as jwt from 'jsonwebtoken';
const jwtBearerToken = jwt.sign(objDesAcl, "cleRandomDApplication", { expiresIn: 120 });
res.cookie("SESSIONID", jwtBearerToken, { httpOnly: true });
```

Le "jwt.sign" crée le JWT. Le premier paramètre est le 'Payload' du JWT (2e partie sur les 3), on lui donne un objet contenant les autorisations, par exemple son ID utilisateur, s'il est admin, etc. Le deuxième paramètre est une clé random, il faudrait l'enregistrer dans un fichier de configuration. Finalement, le 3e paramètre est un objet d'options, dans notre cas le JWT va expirer après 2 minutes (120 secondes).

Ensuite on crée le cookie qui porte le nom "SESSIONID". Le "httpOnly: true" empêche JavaScript d'accéder au cookie, ce qui protège contre les attaques XSS.

C'est possible aussi de passer le JWT via les headers des requêtes (voir: https://blog.angular-university.io/angular-jwt-authentication/), cette autre méthode protège contre le CSRF mais est fragile contre les attaques XSS (l'inverse de la stratégie des cookies).

## Validation du JWT

Sur le serveur, vous pouvez valider l'accès d'un utilisateur avec un "middleware". "middle" car c'est une étape entre le routing et votre contrôleur, on ajoute du code au milieu de votre logique d'application. J'ai donc créé le fichier "middleware/jwtVerification.js", voici le squelette de ce fichier:

```php
import jwt from 'jsonwebtoken';
export const authenticateJWT = (req, res, next) => {
  let token = ...; // Aller chercher le JWT en cookie ou dans le header
  jwt.verify(token, "cleSecreteJwt", (err, payload) => {
    // Gérer l'erreur
    req.payload = payload;
    next();
  });
};
```

Notre fonction authenticateJWT a les mêmes paramètres que nos actions, req (requête) et res (response). Elle a aussi le paramètre next qu'on appel si le JWT est valide.

Pour la fonction jwt.verify: si le JWT n'est pas valide (ou n'est plus valide car il est trop vieux), il va mettre une erreur dans le paramètre next. Si tout est valide, il va mettre le payload du JWT dans le 2e paramètre.

Donc si le JWT est valide, on ajoute le payload à la requête (sera disponible dans la requête pour les contrôleurs) et on appel next. Si le JWT n'est pas valide, envoyer un message d'erreur avec le bon status.

Pour utiliser ce middleware, on le donne en paramètre au routeur (ne pas oublier de l'import):

```js
routeur.get('/admin', authenticateJWT, admin);
```

C'est là que le 'next' du middleware va faire du sens. Si on fait un get sur '/admin', le routeur va appeler 'authenticateJWT'. La fonction 'next' va appeler la prochaine fonction du routeur, donc 'admin'. Si on n'appel pas 'next', le routeur n'appellera pas 'admin'.

## Information de session en Angular

Avec la stratégie du JWT par cookie, nous n'avons pas accès au détail du JWT. Il faut donc que le serveur informe Angular des autorisations et de la durée de vie de la session. Vous avez deux choix (je n'ai pas trouvé de méthode normée):

 1. Mettre un 2e cookie disponible en lecture/écriture qui contient ces informations. Dans le middleware de Express, si le JWT n'est plus valide on peut supprimer ce cookie aussi.
 2. Au login on retourne le JSON des autorisations qu'on garde dans le localstorage.

Si le serveur invalide le JWT et qu'Angular ne peut plus appeler l'API, votre application aura plusieurs erreurs. Une des méthodes est de créer un Intercepteur qui sera en mesure de récupérer tous les appels http qui reçoivent un code HTTP précis. Une autre méthode serait d'utiliser les "guards" (prochain chapitre).

Sources:
https://blog.angular-university.io/angular-jwt-authentication/