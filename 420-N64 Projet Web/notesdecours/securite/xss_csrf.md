# Sécurité

Angular protège déjà de quelques failles:

## XSS

Dans vos vues (fichiers HTML des composants), les moustaches ( {{ et }} ) protègent contre le XSS. Les attributs HTML entre crochets proviennent aussi des variables TypeScripts, mais sont aussi protégés contre le XSS. Angular vous fournis les fonctions 'bypassSecurityTrust...' qui permettent de risquer la sécurité du site.

## CSRF

Angular fait déjà le travail pour vous protéger des attaques CSRF, mais Express ne le fait pas! Express doit donc être configuré pour gérer la protection des CSRF. Premièrement, on va installer "csurf" via npm. csurf est une librairie qui ajoute la gestion de Express dans un serveur Express. Il faut importer csurf et ajouter le middleware après cookieParser, mais avant nos routes:

```js
import csrf from 'csurf';
app.use(csrf({ cookie: { sameSite:true }}), (req, res, next) => {
  res.cookie('XSRF-TOKEN', req.csrfToken(), { httpOnly: false, sameSite: true });
  next();
});
```

Angular demande que le cookie contenant le jeton CSRF s'appel 'XSRF-TOKEN'. Par contre csurf ne met pas le token dans son cookie mais un secret, ce qui est inutile, il faut donc créer le cookie à la main. J'ai mis "sameSite: true" au cookie sinon Firefox n'aimait pas ça...

Par magie, Angular va prendre ce cookie et ajouter le jeton dans le HEADER sous le nom 'X-XSRF-TOKEN'. Csurf va vérifier ce jeton automatiquement sur toutes les requête POST, PUT et DELETE et faire une erreur 500 si le CSRF n'est pas valide.

Notez que pour que le tout fonctionne, il faut utiliser le Proxy de Angular.