# TypeScript

TypeScript permet d'écrire du JavaScript typé. Un fichier TypeScript (fichier *.ts) doit être compilé en fichier JavaScript afin d'être compris par node ou le navigateur.

Webstorm permet de compiler les fichiers TypeScript facilement. Il y a deux méthodes:

 1. Dans le coin en bas à droite de l'IDE, cliquez sur "TypeScript 4.1.3" (le numéro de version peut changer). Sélectionnez Compile et choisissez les fichiers à compiler.
 2. Dans les préférences de l'IDE, dans "Languages & Frameworks" puis dans TypeScript, cochez la case "Recompile on changes".
 
Angular s'occupe de compiler les fichiers TypeScript, donc une étape de moins pour nous!

## Configuration

Il faut un fichier "tsconfig.json" à la racine de votre projet, il va indiquer au compilateur comment transformer le TypeScript en JavaScript. Des fichiers de bases sont données sur le [GitHub de tsconfig](https://github.com/tsconfig/bases/tree/master/bases). Nous allons utiliser le fichier node14.json pour notre serveur qui s'exécutera via node et nous utiliserions recommended.json pour le client qui sera exécuté localement chez les utilisateurs. Par contre, [Angular donne sa propre recommendation de tsconfig.json](https://angular.io/guide/typescript-configuration) que nous allons utiliser pour le client.

Explication des options principales de tsconfig.json et d'options secondaires utilisés par Angular:
 * [target](https://www.typescriptlang.org/tsconfig#target): Version de EcmaScript (JavaScript) après la compilation, il faut utiliser "ES6" au maximum pour le JavaScript client (voir le [support de ES6](https://caniuse.com/?search=es6)). Pour les applications serveurs (node.js), on peut utiliser la dernière version, soit "ES2020". Vous pouvez utiliser "ESNext" qui utilisera la version la plus récente possible, mais c'est déconseillé car vos normes peuvent changer n'importe quand.
 * [lib](https://www.typescriptlang.org/tsconfig#lib): Les librairies utilisés par TypeScript sous forme de tableau. TypeScript s'en sert pour valider votre code. Si vous utilisez "ES2020" dans lib et "ES6" dans target, vous écrivez en ES2020 et le JavaScript compilé sera compatible avec "ES6". Il faut ajouter "dom" dans les lib si votre TypeScript doit modifier du HTML.
 * [module](https://www.typescriptlang.org/tsconfig#module): Comment TypeScript gère les modules. Les applications nodes utilisent "commonjs" qui est une norme propre à node.js. Angular suggère l'utilisation de "es2020", qui est l'utilisation des modules que nous avons vu dans votre précédent cours de Web, avec les import et export. Pour simplifier notre programme, nous allons utiliser "es2020" pour le client et le serveur.
 * [strict](https://www.typescriptlang.org/tsconfig#strict): Validation du typage dans TypeScript, assure aussi que les variables sont déclarés. Mettre à True.
 * [strictPropertyInitialization](https://www.typescriptlang.org/tsconfig#strictPropertyInitialization): Valide que le type des propriétés est valide à l'initialisation. Peut être intéressant de le mettre à False, mais peut causer des erreurs à l'exécution. Par défaut est égale à strict.
 * [sourceMap](https://www.typescriptlang.org/tsconfig#sourceMap): Génère un autre fichier .js pour chaque fichier TS à la compilation, ce fichier fait le lien entre le JS et le TS, utile pour debugger.
 * [declaration](https://www.typescriptlang.org/tsconfig#declaration): Génère un autre fichier .js pour chaque fichier TS à la compilation, ce fichier permet de garder en mémoire le type de chaque variable non-typé. On va garder cette configuration à false, vous devez tout typer!
 * [downlevelIteration](https://www.typescriptlang.org/tsconfig#downlevelIteration): Permet de bien gérer les itérateurs dans ES5 et moins. Angular le met à true mais ce n'est plus très utile... Le code généré est affreux.
 * [experimentalDecorators](https://www.typescriptlang.org/tsconfig#experimentalDecorators): Active les décorateurs dans TS. Un décorateur permet de lier du code à n'importe quoi en ajoutant le décorateur à la déclaration (commence par un @).
 * [moduleResolution](https://www.typescriptlang.org/tsconfig#moduleResolution): "node" ou "classic", node par défaut mais Angular force le "node". Permet de choisir comment TS va chercher les modules lors des imports. "classic" va fouiller dans le dossier actuels et tous les dossiers parents. "node" va fouiller de manière intelligente dans votre projet.
 * [importHelpers](https://www.typescriptlang.org/tsconfig#importHelpers): A true, crée un fichier JavaScript "tslib" qui contiendra toutes les fonctions utilitaires pour passer de la version TS vers la version JS. A false les fonctions utilitaires sont copiés dans tous les fichiers. False par défaut.

## Utilisation

TS est entièrement compatible à JS (selon votre configuration tsconfig.json).

Pour typer un élément en TS, un met un ':' suivit du type immédiatement après le nom de l'élément. Par exemple:

```typescript
let count : number = 0;
```

Les types [disponibles](https://www.typescriptlang.org/docs/handbook/basic-types.html):
 * boolean
 * number, autant pour les int que pour les floats
 * bigint, pour les nombres entiers supérieurs à 2
 * string, vous pouvez utiliser ', " ou ` comme en JS
 * [] après un type de base pour en faire un tableau, exemple: number[]
 * unknown, peut être n'importe type, mais ne permet pas les opérations typés
 * any, peut-être n'importe type, c'est comme une vrai variable JS
 * void, n'a aucune valeur, peut seulement être null, utile pour les valeur de retour
 * null, peut seulement être null
 * undefined, peut seulement être undefined
 * never, sur une valeur de retour, signifie que la fonction ne termine pas normalement, par exemple avec un throw

Autres types:

 * Tuple: Permet de mettre plusieurs types dans un tableau: [string, number]
 * Enum: JS ne permet pas les enums, TS ajoute cette fonctionnalité, voir la doc

On peut aussi combiner des valeurs avec le |, par exemple une variable qui peut être un entier ou null:

```typescript
let id : number | null = null;
```

### Variables

On va toujours déclarer les variables avec le mot-clé let, n'utilisez pas le mot-clé var.

### Classes

Par défaut, les propriétés de classes sont publiques, vous pouvez utilisez les mots-clés public, private et protected.

Vous avez aussi accès aux mots-clés abstract, static, extends, implements, constructor, get, set etc.

### Point-virgule

En JS tout comme en TS, le point-virgule est optionnel. Par contre, sans point virgule c'est JavaScript ou le compilateur qui décide où termine la ligne de code...

De plus, si vous avez une ligne qui débute par (, [ ou `, vous DEVEZ mettre un point-virgule dans la ligne d'avant. Donc tant qu'à mettre des point-virgules "une fois de temps en temps" et d'avoir un code non-uniforme, gardez vos pratique PHP, C# et C++ et utilisez les points-virgules.