# Projet final

## Description
Vous devez développer une application flutter permettant à un client:
* de se connecter (par oauth),
* de rechercher un service ou un dividu à proximité en utilisant la géolocalisation, 
* de télécharger et d'afficher des informations génériques sur le service ou le dividu 
* de filtrer par des paramètres généraux (settings), 
* de commander ou d'utiliser le service,

Le tout doit se faire en simulant l'utilisation sur un téléphone. 

Voici les requis du clients :
1. Connexion,
2. Utilisation d'un périphérique de l'ordinateur
3. Afficher un service, une carte ou des dividus
4. Effectuer choix du service ou des dividus 
5. Filtrer par settings (par exemple ville ou service)
6. Affichage du panier final et de la facture,


## Plateforme de développement
* Flutter pour créer votre solution pour Android et iOS. 
* Vous devez choisir une architecture pour votre projet afin de rendre votre application plus robuste à long terme, faciliter les tests et partager mieux les tâches. Voir le ![Architecture](../Notes%20de%20Cours/Cours%205/1.%20Architecture.md) pour des suggestions.
* Rappelez-vous que vous pouvez tester votre développement avec Chrome, mais je vais corriger sur Android et sur iPhone.

## Authentification
Vous devrez permettre un système d'authentification par OAuth (minimalement Apple, Facebook ou Google).

## Interface offerte
Un Pixel 7 Pro sera utilisé pour valider le fonctionnement de votre application. Il faudra offrir une interface native pour les deux principaux systèmes d’exploitation :
* Cupertino pour iOS
* Material pour Android

## Recherche 
* Vous devez déterminer le service ou le dividu le plus près. 

## Visionnement d’un service

* Afficher plusieurs images du service dans un carrousel. 
* Présentation de l'application sera évaluée

## Préférence
Offrez une interface de préférence afin que l'utilisateur puisse filtrer les résultats avant même que ça lui parvient. Par exemple, il pourrait décider de covoiturer uniquement les personnes non genrées.

## Utilisation
Les commandes complétées sont enregistrées sur une base de donnée Firebase à distance

## Images
Pour éviter le téléchargement à répétition des images de l'application, veuillez utiliser: https://pub.dev/packages/cached_network_image.

## Tests d’intégration / unitaires
Un ensemble de tests unitaires doivent être réalisés :
* Validation du JSON transféré à partir du serveur
* Ajout dans le panier, mise à jour du nombre d'éléments dans le panier.
* Comportement approprié lors du calcul du total de la facture.
 
## Remise
On procèdera en mode Sprint, 4 sprints :
* 6 au 13 février 
* 13 au 20 février
* 20 février au 1er mars

## Évaluation du projet

| **Note individuelle** | **/70** |
| --------------------- | ------- |
| Oral                  | /15     |
| Évaluation des pairs  | /5      |
| **Sprint**            | **/50** |
| Sprint 1              | /5      |
| Sprint 2              | /10     |
| Sprint 3              | /10     |
| Sprint 4              | /25     |

| **Note de groupe**           | **/20** |
| ---------------------------- | ------- |
| Planification / Modélisation | /20     |

## Évaluation lors des sprints

| Titre                                            | Évaluation |
| ------------------------------------------------ | ---------- |
| Qualité du code                                  | 50%        |
| Participation dans l’équipe (respect des normes) | 50%        |

* La qualité du code ne peut dépasser la note de participation, donc si vous contribuez à 10%, vous pouvez avoir au maximum 10% comme note de qualité du code. Rappelez-vous que j’évalue de façon individuelle, je m'attends que vous fassiez chacun des commits dans l'arbre.

* Je veux savoir à chaque instant: 
  * les tâches à compléter pour l'équipe (total et pour le sprint en cours),
  * le ou les tâches qui vous sont assingés individuellement,
  * les tâches complétées (par un sprint antérieur),
  * les tâches à complétez pour le prochain sprint (pour chaque membre de l'équipe)

## Sprint 4 (final)

| **Critères** | **/65** |
| ----------------------------------- | ------- |
| Connexion                           | /10      |
| Recherche / Affichage (images)      | /10     |
| Réservation / Panier                | /10     | 
| Préférences  (Respect des préférences) | /10     |
| GPS (Périphérique),  Cartes         | /10     |
| Interface globale, Apparaence, (iOS, Material)   | /10     | 
| Respect des normes de développement | /5 | 


## Oral

L'oral devra être réalisé sur les points suivants :
1. Démonstration du projet
2. Quel framework avez-vous utiliser pour votre développement et pourquoi
3. Quelles sont les étapes qui vous ont causées le plus de problème et comment les avez-vous résolu?
4. Si c'était à refaire, que changeriez-vous dans votre organisation
5. Tout autre sujet que vous désirez discuter.