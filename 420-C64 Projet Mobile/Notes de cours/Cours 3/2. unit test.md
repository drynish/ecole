# Les tests unitaires

Les tests unitaires dans Flutter sont essentiels pour garantir la qualité et la fiabilité de vos applications. Ils permettent de tester des portions isolées de code, comme des fonctions, des méthodes ou des classes, afin de s'assurer qu'elles fonctionnent correctement dans diverses conditions.

**Pourquoi écrire des tests unitaires dans Flutter ?**

*   **Détection précoce des erreurs :** Les tests unitaires permettent de détecter les erreurs dès le début du processus de développement, ce qui est beaucoup plus efficace que de les découvrir lors des tests d'intégration ou après la mise en production.
*   **Refactorisation plus sûre :** Lorsque vous modifiez ou refactorisez du code, les tests unitaires vous assurent que les modifications n'ont pas introduit de nouvelles erreurs.
*   **Documentation du code :** Les tests unitaires servent de documentation en montrant comment le code est censé se comporter.
*   **Amélioration de la conception :** L'écriture de tests unitaires encourage une conception modulaire et testable du code.

**Le package `flutter_test`**

Flutter est livré avec un package de test intégré appelé `flutter_test`. Ce package fournit les fonctionnalités de base nécessaires pour écrire et exécuter des tests unitaires dans Flutter. Il est inclus par défaut dans chaque projet Flutter, il n'y a donc pas de configuration supplémentaire nécessaire.

**Structure d'un test unitaire**

Un test unitaire typique dans Flutter suit généralement cette structure :

1.  **Importation des packages nécessaires :**

    ```dart
    import 'package:flutter_test/flutter_test.dart';
    // Importez le fichier contenant le code à tester
    import 'package:votre_projet/votre_fichier.dart';
    ```

2.  **Définition du groupe de tests (facultatif mais recommandé) :**

    ```dart
    void main() {
      group('Groupe de tests pour VotreClasse', () {
        // Les tests unitaires seront ici
      });
    }
    ```

3.  **Écriture des tests avec `test()` :**

    ```dart
    test('Description du test', () {
      // Préparation : Initialisation des données et des objets nécessaires
      final maClasse = VotreClasse();

      // Action : Appel de la méthode à tester
      final resultat = maClasse.maMethode(parametre1, parametre2);

      // Assertion : Vérification du résultat attendu
      expect(resultat, expectedResult);
    });
    ```

**Exemple concret**

Supposons que vous ayez une fonction qui additionne deux nombres :

```dart
int additionner(int a, int b) {
  return a + b;
}
```

Voici un exemple de test unitaire pour cette fonction :

```dart
import 'package:flutter_test/flutter_test.dart';
import 'votre_fichier.dart'; // Importez le fichier contenant la fonction additionner

void main() {
  group('Tests pour la fonction additionner', () {
    test('Addition de deux nombres positifs', () {
      expect(additionner(2, 3), 5);
    });

    test('Addition d\'un nombre positif et d\'un nombre négatif', () {
      expect(additionner(5, -2), 3);
    });

    test('Addition de deux nombres négatifs', () {
      expect(additionner(-1, -4), -5);
    });

    test('Addition de zéro', () {
        expect(additionner(5, 0), 5);
    });

  });
}
```

**Exécution des tests**

Pour exécuter les tests unitaires dans Flutter, vous pouvez utiliser la commande suivante dans le terminal :

```bash
flutter test
```

Cette commande exécute tous les tests présents dans le répertoire `test/` de votre projet.

**Assertions courantes**

Le package `flutter_test` fournit plusieurs fonctions d'assertion pour vérifier les résultats des tests. Voici quelques-unes des plus courantes :

*   `expect(actual, matcher)` : Vérifie que la valeur `actual` correspond au `matcher`.
*   `equals(expected)` : Vérifie que la valeur est égale à `expected`.
*   `isTrue` : Vérifie que la valeur est vraie.
*   `isFalse` : Vérifie que la valeur est fausse.
*   `isNull` : Vérifie que la valeur est nulle.
*   `isNotNull` : Vérifie que la valeur n'est pas nulle.
*   `throwsException` : Vérifie qu'une exception est levée.

**Bonnes pratiques**

*   **Écrire les tests avant le code (TDD) :** Bien que ce ne soit pas toujours possible, écrire les tests avant d'implémenter le code permet de mieux définir les exigences et de concevoir un code plus testable.
*   **Tester les cas limites et les cas d'erreur :** Ne testez pas seulement les cas nominaux, mais aussi les cas limites (par exemple, les valeurs nulles, les chaînes vides, les nombres négatifs) et les cas d'erreur.
*   **Utiliser des noms de tests descriptifs :** Des noms de tests clairs et descriptifs facilitent la compréhension des tests et l'identification des erreurs.
*   **Isoler les tests :** Chaque test doit être indépendant des autres et ne pas dépendre d'un état global.
*   **Utiliser des mocks pour les dépendances externes :** Si votre code dépend de services externes (par exemple, une API), utilisez des mocks pour simuler ces services et isoler le code que vous testez.

