## Outil de ligne de commande

flutter vient avec un logiciel de ligne de commande vous permettant certaines fonctionnalités: 

![](images/Flutter.png)

Les commandes principales pour vous aider à partir:

* create: permet de créer un projet flutter
* doctor: permet de savoir si votre installation de flutter est fonctionnelle.
* logs: permet de voir les messages en console
* pub: permet de gérer les packets
* run: permet de lancer l'exécution
* emulators: permet de lancer un émulateur.
* install: permet d'installer sur un émulateur
* test: permet de lancer les tests unitaires de votre projet en cours.
  
### ios

Vous devez utiliser un ordinateur mac pour compiler sur mac. Dans votre projet flutter créé, vous aurez un répertoire ios que vous pourriez ouvrir avec xcode pour la compilation d'un projet flutter.

## Exécution

Puisque c'est un cours de mobile, il est important d'exécuter sur Android à la fin, pour le développement, vous pouvez utiliser l'exécution en Google Chrome car celle-ci est généralement la plus rapide. Simplement exécuter **flutter run** et choisir web.

## Commande create

![](images/Projet%20flutter.png)

* android / ios / web / windows / mac / linux: les répertoires respectifs des différents plateformes.
* lib: projet principal de flutter avec les différents fichiers .dart nécessaire pour votre projet.
* pubspec.xml: contient les informations de votre paquetage dart, tout comme les dépences externes, images, fonts et plus.
* README: contient l'information de votre projet.
* test: contient l'information pour les tests unitaires de votre projet.

