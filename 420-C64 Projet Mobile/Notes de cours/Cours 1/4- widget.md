# Tout est un widget, presque.

Le concept de widget est fondamental dans Flutter. Presque tout est un widget, de l'application elle-même aux plus petits éléments d'interface utilisateur. Cette architecture permet une grande flexibilité et une composition modulaire de l'interface.

## Qu'est-ce qu'un Widget ?

Un widget est une description immuable d'une partie de l'interface utilisateur. Il décrit à quoi doit ressembler une partie de l'écran, et non comment elle doit être dessinée. Flutter se charge ensuite de la traduction en rendu visuel.

Un widget peut représenter :

  * **Un élément visuel :** `Text`, `Image`, `Icon`, `Button`
  * **Un élément de mise en page (layout) :** `Row`, `Column`, `Padding`, `Container`
  * **Un élément de style :** `Theme`, `TextStyle`, `Colors`
  * **Un élément d'interaction :** `GestureDetector`, `InkWell`

L'idée clé est de composer des widgets imbriqués pour construire des interfaces utilisateur complexes. Chaque widget a une responsabilité unique, ce qui favorise la réutilisabilité et la maintenabilité du code.

![](images/Widget.png)
![](images/Arbre%20widget.png)

**Types de Widgets : Stateless et Stateful**

Il existe deux types principaux de widgets :

  * **StatelessWidget :** Ces widgets sont immuables. Une fois construits, ils ne peuvent pas être modifiés. Ils sont utiles pour les parties statiques de l'interface utilisateur qui ne changent pas en réponse aux interactions de l'utilisateur ou aux changements de données.

  * **StatefulWidget :** Ces widgets sont dynamiques et peuvent changer d'état au cours de la vie de l'application. Ils sont utilisés pour les parties de l'interface utilisateur qui doivent se mettre à jour en réponse à des événements.

**StatelessWidget**

Un `StatelessWidget` est simple. Il ne possède pas d'état interne. Son apparence est entièrement déterminée par les informations qu'il reçoit de son parent.

```dart
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const ImmutableWidget(),
    );
  }
}

class ImmutableWidget extends StatelessWidget {
  const ImmutableWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
      padding: const EdgeInsets.all(40),
      child: Container(
        color: Colors.purple,
        padding: const EdgeInsets.all(50.0),
        child: Container(
          color: Colors.blue,
        ),
      ),
    );
  }
}
```

Dans cet exemple, `ImmutableWidget` est un `StatelessWidget`. Sa construction est simple et son apparence ne changera pas.

**StatefulWidget**

Un `StatefulWidget` est plus complexe. Il est composé de deux classes :

  * **StatefulWidget :** La classe widget elle-même. Elle est immuable et décrit la configuration du widget.
  * **State :** La classe d'état associée. Elle contient les données qui peuvent changer et qui affectent l'apparence du widget.

<!-- end list -->

```dart
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium, // Utilisation de headlineMedium pour une meilleure lisibilité
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
```

**Explication du code Stateful:**

1.  **`createState()` :** La méthode `createState()` de `MyHomePage` crée une instance de la classe `_MyHomePageState`.

2.  **`_MyHomePageState` :** Cette classe contient l'état mutable du widget (`_counter`).

3.  **`setState()` :** La méthode `setState()` est cruciale. Elle informe Flutter que l'état du widget a changé. 

**Pourquoi `setState()` est-il important ?**

`setState()` ne se contente pas de mettre à jour la variable `_counter`. Il déclenche également une reconstruction du widget. C'est ce processus de reconstruction qui met à jour l'interface utilisateur. Sans `setState()`, les changements de `_counter` ne seraient pas visibles à l'écran.

**Arbre des Widgets (Widget Tree)**

L'interface utilisateur de Flutter est construite sous forme d'arbre de widgets. Chaque widget est un nœud de cet arbre. Le widget racine est généralement `MaterialApp` ou `CupertinoApp`. La structure imbriquée des widgets définit la hiérarchie et la mise en page de l'interface utilisateur.

**Ressources supplémentaires:**

  * **Documentation officielle des widgets Flutter :** [https://docs.flutter.dev/ui/widgets](https://docs.flutter.dev/ui/widgets)

  * **Catalogue des widgets Flutter :** [https://api.flutter.dev/flutter/widgets/widgets-library.html](https://www.google.com/url?sa=E&source=gmail&q=https://api.flutter.dev/flutter/widgets/widgets-library.html)

Cette mise à jour fournit une explication plus claire et plus complète des widgets Flutter, en mettant l'accent sur la distinction entre `StatelessWidget` et `StatefulWidget` et en expliquant le rôle crucial de `setState()`. J'ai également inclus des liens vers la documentation officielle pour plus d'informations. J'ai aussi simplifié et modernisé le code, en utilisant `const` là où c'était approprié et en utilisant `headlineMedium` pour le style du texte, ce qui est plus conforme aux recommandations actuelles de Flutter.
