# Dart

https://dart.dev/#try-dart
https://dart.dev/language/built-in-types#strings

## Introduction à Dart pour Flutter : Avantages et Inconvénients comparés à Kotlin et Swift

Dart est un langage de programmation open source développé par Google, initialement conçu pour le développement web, mais qui a trouvé une application majeure avec Flutter, le framework de Google pour la création d'applications multiplateformes (iOS, Android, Web, Desktop). Dart est un langage orienté objet avec une syntaxe qui rappelle C, Java et JavaScript, ce qui le rend relativement facile à apprendre pour les développeurs familiers avec ces langages.

## Pourquoi Dart a été choisi pour Flutter ?

Plusieurs raisons ont motivé le choix de Dart pour Flutter :

*   **Performances:** Dart compile en code machine natif (AOT - Ahead-of-Time) pour les versions release des applications, ce qui offre des performances proches du natif sur les appareils mobiles. En mode développement, il utilise la compilation JIT (Just-in-Time) pour un rechargement à chaud (Hot Reload) très rapide, ce qui accélère considérablement le cycle de développement.
*   **Productivité:** Le Hot Reload permet aux développeurs de voir instantanément les modifications apportées au code dans l'application en cours d'exécution, sans avoir à la redémarrer. Ceci améliore grandement la productivité et l'itération rapide.
*   **Gestion de l'interface utilisateur (UI):** Dart est optimisé pour la création d'interfaces utilisateur réactives. Son système de mise en page basé sur les widgets et son exécution rapide permettent de créer des animations fluides et des interfaces utilisateur complexes.
*   **Facilité d'apprentissage:** Sa syntaxe familière le rend accessible aux développeurs venant d'autres langages.
*   **Support de Google:** Étant développé par Google, Dart bénéficie d'un support et d'une évolution constants, ce qui garantit sa pérennité et son adaptation aux nouvelles technologies.

## Avantages de Dart par rapport à Kotlin et Swift:

*   **Hot Reload plus performant:** Bien que Kotlin et Swift proposent des fonctionnalités similaires, le Hot Reload de Dart est souvent considéré comme plus rapide et plus fiable, ce qui est un atout majeur pour le développement rapide d'interfaces utilisateur.
*   **Compilation AOT et JIT:** Dart combine les avantages de la compilation AOT pour la performance en production et de la compilation JIT pour le développement rapide.
*   **Framework intégré (Flutter):** Dart est intrinsèquement lié à Flutter, ce qui permet une intégration plus profonde et une optimisation conjointe.

## Inconvénients de Dart par rapport à Kotlin et Swift:**

*   **Écosystème plus restreint:** L'écosystème de Dart, bien qu'en croissance, est plus petit que ceux de Kotlin (avec l'écosystème Java) et de Swift (avec l'écosystème Apple). Cela signifie qu'il peut y avoir moins de bibliothèques et d'outils disponibles pour certaines tâches spécifiques.
*   **Moins utilisé en dehors du mobile:** Kotlin est largement utilisé pour le développement backend (avec Spring) et Swift gagne en popularité pour le développement côté serveur. Dart est principalement associé au développement mobile avec Flutter, ce qui limite son champ d'application.
*   **Adoption moins large:** Kotlin est le langage privilégié pour le développement Android natif et Swift pour iOS. Dart n'a pas cette même position dominante dans un écosystème natif.

**Tableau comparatif simplifié:**

| Caractéristique       | Dart                                   | Kotlin                                      | Swift                                      |
| --------------------- | -------------------------------------- | ------------------------------------------- | ------------------------------------------ |
| Principal usage       | Développement mobile multiplateforme (Flutter) | Développement Android natif, backend, multiplateforme | Développement iOS/macOS natif, backend |
| Compilation           | AOT et JIT                             | AOT (Android natif), JVM bytecode          | AOT                                        |
| Hot Reload            | Très rapide et fiable                 | Disponible, mais parfois moins stable          | Disponible                                 |
| Écosystème            | En croissance, plus restreint          | Très large (basé sur Java)                   | Large (basé sur l'écosystème Apple)        |
| Courbe d'apprentissage | Relativement facile pour les développeurs familiers avec C, Java, JavaScript | Facile pour les développeurs Java           | Relativement facile, syntaxe moderne        |

**Conclusion:**

Dart est un langage puissant et performant, particulièrement adapté au développement d'applications mobiles multiplateformes avec Flutter. Ses avantages en termes de productivité (Hot Reload) et de performances en font un choix pertinent pour de nombreux projets. Bien que son écosystème soit plus petit que ceux de Kotlin et Swift, il continue de croître et de s'améliorer. Le choix entre Dart, Kotlin et Swift dépendra des besoins spécifiques du projet, de l'expérience de l'équipe de développement et des plateformes ciblées.


## Hello World
```dart
void helloDart(String name) {      
  print('Hello, $name');
}

void main() {
  List<String> greetings = [    
    'World',
    'Mars',
    'Oregon',
    'Barry',
    'David Bowie',
  ];

  for (var name in greetings) {     
    helloDart(name);                
  }
}
```

## I/O

```dart
import 'dart:io';

void main() {
  stdout.writeln('Entrez le nom');
  String? input = stdin.readLineSync(); 
  return helloDart(input);
}

void helloDart(String? name) {
  print('Hello, $name');
}
```

https://api.flutter.dev/flutter/dart-io/Stdin/readLineSync.html

## Principe de base

* Dart est un langage orienté objet et supporte l'héritage unique.
* En Dart, tout est un objet et chaque objet est une instance d'une classe. Chaque objet hérite de la classe Object. Même les nombres sont des objets, pas des types primitifs.
* Dart est typé. Vous ne pouvez retourner un entier d'une méthode qui devrait retourner une chaîne.

![](images/Dart-platforms.svg)

## Types de base

```dart
// Équivalent à String name = 'Voyager I';
// Il s'agit d'une inférence de type (détection du type)
var name = 'Voyager I';
var year = 1977;
var antennaDiameter = 3.7;
var flybyObjects = ['Jupiter', 'Saturn', 'Uranus', 'Neptune'];
var image = {
  'tags': ['saturn'],
  'url': '//path/to/saturn.jpg'
};
```

###  Const ou final

De façon générale, les deux déclarations créent des variable immuable:
* Un const est une variable définie et invariable. 
* Un final doit être assignée dans le même bout de code où elle a été déclarée.
* Il y a de l'optimisation CPU lorsque vous utilisez les variables const; moins de mémoire est utilisée.
* 
```dart

final int numValue = 42; // ceci est ok
// NOT OK: const int or var int.

numValue = 64; // This will throw an error
```



```dart
// Dans une liste cependant, la liste est invariable, mais on peut lui ajouter des éléments
final fruit = ["apple", "pear", "orange"];
fruit.add("grape");

// vu que la liste est constante, on ne peut la mutater (modifier)
final cars = const ["Honda", "Toyota", "Ford"];

// const nécessite un assignement constant, là où final accepte const ou non:
const names = const ["John", "Jane", "Jack"];



```

### num

```dart
num x = 1; // x peut avoir des entiers ou des doubles comme valeur
x += 2.5;
```

## String

```dart
// Interpolation
final sum = 1 + 1;
final interpolate = 'one plus one is $sum'

// Évaluation ternaire
final age = 45;
final howOld = 'J'ai $age ${age == 1 || age == 0 ? 'an' : 'ans'}.';
print(howOld);

// String -> int
var one = int.parse('1');
assert(one == 1);

// String -> double
var onePointOne = double.parse('1.1');
assert(onePointOne == 1.1);

// int -> String
String oneAsString = 1.toString();
assert(oneAsString == '1');

// double -> String
String piAsString = 3.14159.toStringAsFixed(2);
assert(piAsString == '3.14');
```

L'interpolation est un moyen de faire de la concatenation mais si vous avez une multitude de chaîne à concaténer, vaut mieux utiliser le StringBuffer:

```dart
List fruits = ['Strawberry', 'Coconut', 'Orange', 'Mango', 'Apple'];
StringBuffer buffer = StringBuffer();
for (String fruit in fruits) {
  buffer.write(fruit);
  buffer.write(' ');
}
print (buffer.toString()); // affiche: Strawberry Coconut Orange Mango Apple
```

### Type Dynamic

Dart supporte également le typage dynamique, ce qui permet d'éviter de valider le type des variables lors de la compilation.

```dart
dynamic myNumber = 'Hello';
```

### Utilisation du type dynamique

Il est généralement recommandé de ne pas l'utiliser

## Condition

```dart
if (year >= 2001) {
  print('21st century');
} else if (year >= 1901) {
  print('20th century');
}

for (final object in flybyObjects) {
  print(object);
}

for (int month = 1; month <= 12; month++) {
  print(month);
}

while (year < 2016) {
  year += 1;
}
```

## Fonction

```dart

optionalReturnType functionName(optionalType parameter1, optionalType parameter2...) {
  // code
} 

int fibonacci(int n) {
  if (n == 0 || n == 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}

var result = fibonacci(20);
```

### Paramètres nommés

```dart
void named({String? greeting, String? name}) {
  final actualGreeting = greeting ?? 'Allô';
  final actualName = name ?? 'Personne mystère';
  print('$actualGreeting, $actualName!');
}

named(greeting: 'Bien le bonjour');
named(name: 'Sonia');
named(name: 'Alex', greeting: 'Bonjour');

//Bien le bonjour, personne mystère
//Hello, Sonia!
//Bonjour, Alex!
```

Ceci sera particulièrement utile à l'intérieur de cadriciel Flutter:
```dart
Container(
  margin: const EdgeInsets.all(10.0), 
  color: Colors.red, 
  height: 48.0,
  child: Text('Named parameters are great!'),
)
```
Imaginez la même situation sans les paramètres nommés, on devrait se rappeler pour chaque constructeur de l'ordre des éléments. L'avantage ici, c'est que même mélangé, ça reste valide.

### Closures

L'idée ici est de mettre une fonction dans la définition d'une autre fonction.

```dart
// Ceci définit un type nommée NumberGetter qui représente une fonction retournant un entier
typedef NumberGetter = int Function();

// La méthode suivante va recevoir une fonction comme argument et retourner le carré de cette méthode
int powerOfTwo(NumberGetter getter) {
 return getter() * getter();
}

void consumeClosure() {
 final getFour = () => 4; // fonction anonyme qui retourne 4
 final squared = powerOfTwo(getFour);
 print(squared);
}
```

#### Symbole =>

Ce symbole permet de spécifier une fonction inline:

```dart
final getFour = () {
  return 4;
};

final getFour = () => 4;
```

## Classes

Voici ce qui n'est pas disponible en DART:

* private
* protected
* public
* struct
* interface
* protocol

```dart
class Name {
 final String first;
 final String last;

 Name(this.first, this.last);


 @override
 String toString() {
  return '$first $last';
 }
}
```

### Héritage

```dart
class OfficialName extends Name {
 // Les propriétés privées commencent avec un _
 final String _title;
 
 // You can add colons after constructor
 // to parse data or delegate to super
 OfficialName(this._title, String first, String last) : super(first, last);

 @override
 String toString() {
   return '$_title. ${super.toString()}';
 }
}

final name = OfficialName('Mr', 'Michel', 'Di Croci');
final message = name.toString();
print(message);
```

### Relations entre les classes

* extends : Héritage
* implements: Inteface
* with: Mixin (en Dart, il est possible de créer du code disponible pour plein de classes sans utiliser l'héritage)

## Collections

### List

Liste d'élément où l'ordre est important

### Maps

Ensemble d'élément non linéaire où chaque élément est accédé par une clé

### Sets

Ensemble d'élément unique non linéaire

#### Mapping

```dart
List<Map> data = [
 {'first': 'Nada', 'last': 'Mueller', 'age': 10},
 {'first': 'Kurt', 'last': 'Gibbons', 'age': 9},
 {'first': 'Natalya', 'last': 'Compton', 'age': 15},
 {'first': 'Kaycee', 'last': 'Grant', 'age': 20},
 {'first': 'Kody', 'last': 'Ali', 'age': 17},
 {'first': 'Rhodri', 'last': 'Marshall', 'age': 30},
 {'first': 'Kali', 'last': 'Fleming', 'age': 9},
 {'first': 'Steve', 'last': 'Goulding', 'age': 32},
 {'first': 'Ivie', 'last': 'Haworth', 'age': 14},
 {'first': 'Anisha', 'last': 'Bourne', 'age': 40},
 {'first': 'Dominique', 'last': 'Madden', 'age': 31},
 {'first': 'Kornelia', 'last': 'Bass', 'age': 20},
 {'first': 'Saad', 'last': 'Feeney', 'age': 2},
 {'first': 'Eric', 'last': 'Lindsey', 'age': 51},
 {'first': 'Anushka', 'last': 'Harding', 'age': 23},
 {'first': 'Samiya', 'last': 'Allen', 'age': 18},
 {'first': 'Rabia', 'last': 'Merrill', 'age': 6},
 {'first': 'Safwan', 'last': 'Schaefer', 'age': 41},
 {'first': 'Celeste', 'last': 'Aldred', 'age': 34},
 {'first': 'Taio', 'last': 'Mathews', 'age': 17},
];

// Pour transformer cette liste de JSON en donnée on devrait normalement faire, nous reviendrons plus tard sur l'optimisation du transfert lorsque nous couvrirons JSON:

final names = <Name>[];
for (Map rawName in data) {
  final first = rawName['first'];
  final last = rawName['last'];
  final name = Name(first, last);
  names.add(name);
}

// Mais à la place nous ferons uniquement ceci
final names = data.map<Name>((Map rawName) {
  final first = rawName['first'];
  final last = rawName['last'];
  return Name(first, last);
}).toList();

// Maintenant que c'est typé, on peut trier.
names.sort((a, b) => a.last.compareTo(b.last));

print('');
print('Alphabetical List of Names');
names.forEach(print);

// ou filtrer
final onlyMs = names.where((name) => name.last.startsWith('M'));

print('');
print('Filters name list by M');
onlyMs.forEach(print);

// ou merger les données
final allAges = data.map<int>((person) => person['age']);
final total = allAges.reduce((total, age) => total + age);
final average = total / allAges.length;

print('La moyenne d'âge est $average');
```

### Fonction first-class

```dart
// Qu'est-ce que ceci?
names.forEach(print);

// Pourquoi doit-on appeler ceci à la fin du map?
.toList();
```

Comme la méthode forEach() s'attend à une fonction ayant la signature suivante:

`void Function<T>(T element)`

Et que la méthode print() à la signature suivante:

`void Function(Object object)`

Ce qui permet de faire ceci:

```dart
data.forEach((value) {
  print(value);
});

// En une seule ligne de code
data.forEach(print);
```

Si vous regarder le code source des méthodes `map` et `where` que nous avons utiliser précédemment, vous remarqueriez que le retour de ces méthodes ne seraient pas des listes, mais une classe abstraite nommée Iterable. Cette classe est s'agit d'un état intermédiaire avant de décider où vous allez stocker les données. Il n'est pas nécessaire que ce soit une Liste, ça l'aurait pu être un Set par exemple. L'avantage principal d'utiliser un Iterables est qu'il est lazy, i-e que sa valeur ne sera pas calculée avant son utilisation, pas avant, ce qui permet de mettre en chaîne plusieurs opérations avant de faire l'opération finale:

```dart
final names = data
    .map<Name>((raw) => Name(raw['first'], raw['last']))
    .where((name) => name.last.startsWith('M'))
    .where((name) => name.first.length > 5)
    .toList(growable: false);
```

### Opérateur cascade

Création d'un patron de conception bâtisseur qui créer un URL

#### Sans l'opérateur cascade

```dart
class UrlBuilder {
  String _scheme;
  String _host;
  String _path;

  UrlBuilder setScheme(String value) {
    _scheme = value;
    return this;
  }

  UrlBuilder setHost(String value) {
    _host = value;
    return this;
  }

  UrlBuilder setPath(String value) {
    _path = value;
    return this;
  }

  String build() {
    assert(_scheme != null);
    assert(_host != null);
    assert(_path != null);

    return '$_scheme://$_host/$_path';
  }
}

void main() {
  final url = UrlBuilder()
      .setScheme('https')
      .setHost('dart.dev')
      .setPath('/guides/language/language-tour#cascade-notation-')
      .build();
  
  print(url);
}
```

#### Avec cascade

```dart
class UrlBuilder {
  String scheme;
  String host;
  List<String> routes;

  @override
  String toString() {
    assert(scheme != null);
    assert(host != null);
    final paths = [host, if (routes != null) ...routes];
    final path = paths.join('/');

    return '$scheme://$path';
  }
}

final url = UrlBuilder()
  ..scheme = 'https'
  ..host = 'dart.dev'
  ..routes = [
    'guides',
    'language',
    'language-tour#cascade-notation-',
  ];

print(url);
```

L'opérateur cascade n'est pas utile seulement dans ce cas, il peut manipuler des éléments:

```dart
// Au lieu de faire ceci:

final numbers = [342, 23423, 53, 232, 534];
numbers.insert(0, 10);
numbers.sort((a, b) => a.compareTo(b));

// On fait ça:

final numbers = [342, 23423, 53, 232, 534]
    ..insert(0, 10)
    ..sort((a, b) => a.compareTo(b));
  
print('The largest number in the list is ${numbers.last}');

```


## Enum

```dart
enum PlanetType { terrestrial, gas, ice }

enum Planet {
  mercury(planetType: PlanetType.terrestrial, moons: 0, hasRings: false),
  venus(planetType: PlanetType.terrestrial, moons: 0, hasRings: false),
  uranus(planetType: PlanetType.ice, moons: 27, hasRings: true),
  neptune(planetType: PlanetType.ice, moons: 14, hasRings: true);

  /// A constant generating constructor
  const Planet(
      {required this.planetType, required this.moons, required this.hasRings});

  /// All instance variables are final
  final PlanetType planetType;
  final int moons;
  final bool hasRings;

  /// Enhanced enums support getters and other methods
  bool get isGiant =>
      planetType == PlanetType.gas || planetType == PlanetType.ice;
}



final yourPlanet = Planet.earth;

if (!yourPlanet.isGiant) {
  print('Your planet is not a "giant planet".');
}
```

Pour en lire plus:

https://dart.dev/guides/language/language-tour
