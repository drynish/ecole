import './name.dart';
import './picture.dart';

class user {
  final String gender;
  final name yourname;
  final String email;
  final String phone;
  final String cell;
  final picture pictures;

  const user({
    required this.gender,
    required this.yourname,
    required this.email,
    required this.phone,
    required this.cell,
    required this.pictures,
  });

  factory user.fromJson(Map<String, dynamic> json) {
    return user(
      gender: json['gender'],
      yourname: name.fromJson(json['name']),
      email: json['email'],
      phone: json['phone'],
      cell: json['cell'],
      pictures: picture.fromJson(json['picture']),
    );
  }
}
