import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models/user.dart';
import 'dart:convert';
import 'dart:math';

void main() {
  runApp(const MonAppRecherche());
}

class MonAppRecherche extends StatelessWidget {
  const MonAppRecherche({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MonAppRechercheFul(title: 'Recherche utilisateur'),
    );
  }
}

class MonAppRechercheFul extends StatefulWidget {
  const MonAppRechercheFul({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MonAppRechercheFul> createState() => _MonAppRechercheFul();
}

class _MonAppRechercheFul extends State<MonAppRechercheFul> {
  late Future<user> _user;
  String gender = "Homme";

  @override
  void initState() {
    super.initState();
    gender = "Homme";
    _user = fetchUser("male");
  }

  void _onPressed() {
    String apiGender = gender == "Homme" ? "male" : "female";
    setState(() {
      _user = fetchUser(apiGender);
    });
  }

  Future<user> fetchUser(String apiGender) async {
    final response = await http
        .get(Uri.parse('https://randomuser.me/api/?gender=' + apiGender));

    if (response.statusCode == 200) {
      final json = response.body;
      final extractedData = jsonDecode(json);

      List users = extractedData["results"];
      return user.fromJson(users[0]);
    } else {
      throw Exception(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            DropdownButton<String>(
              value: gender,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              style: const TextStyle(color: Colors.blueGrey),
              underline: Container(
                height: 2,
                color: Colors.blueGrey,
              ),
              onChanged: (String? newValue) {
                setState(() {
                  gender = newValue!;
                });
              },
              items: <String>['Homme', 'Femme']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
            TextButton(
                child: const Icon(
                  Icons.person_search,
                ),
                onPressed: _onPressed),
          ]),
          FutureBuilder<user>(
              future: _user,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return const Column(children: []);
                  case ConnectionState.waiting:
                    return const Column(children: [
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: CircularProgressIndicator(),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Text('Attente des données...'),
                      ),
                    ]);
                  default:
                    if (snapshot.hasData) {
                      return Card(
                        child: Row(children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: SizedBox(
                              width: 200.0,
                              height: 200.0,
                              child: Image.network(
                                  snapshot.data!.pictures.large,
                                  fit: BoxFit.fill),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: SizedBox(
                                width: 300,
                                child: Column(children: [
                                  Text(snapshot.data!.yourname.toString(),
                                      style: const TextStyle(fontSize: 25)),
                                  Text(snapshot.data!.email,
                                      style: const TextStyle(fontSize: 10)),
                                  Text(snapshot.data!.cell)
                                ]),
                              ),
                            ),
                          ),
                        ]),
                      );
                    } else {
                      return const Column(children: [
                        SizedBox(
                          width: 60,
                          height: 60,
                          child: CircularProgressIndicator(),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                          child: Text('Erreur...'),
                        ),
                      ]);
                    }
                }
              })
        ]),
      ),
    );
  }
}
