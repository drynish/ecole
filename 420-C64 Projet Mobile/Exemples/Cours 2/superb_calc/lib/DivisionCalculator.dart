import 'package:flutter/material.dart';

class DivisionCalculator extends StatefulWidget {
  const DivisionCalculator({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<DivisionCalculator> createState() => _DivisionCalculator();
}

class _DivisionCalculator extends State<DivisionCalculator> {
  TextEditingController controllerNumber1 = TextEditingController();
  TextEditingController controllerNumber2 = TextEditingController();
  double div = 0;

  void _recalculateNumber(String text) {
    setState(() {
      var number1 = int.tryParse(controllerNumber1.text) ?? 0;
      var number2 = int.tryParse(controllerNumber2.text) ?? 0;

      div = number1 / number2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextFormField(
          controller: controllerNumber1,
          keyboardType: TextInputType.number,
          onChanged: _recalculateNumber,
        ),
        TextFormField(
          controller: controllerNumber2,
          keyboardType: TextInputType.number,
          onChanged: _recalculateNumber,
        ),
        Text('$div'),
      ],
    );
  }
}
