import 'package:flutter/material.dart';

class AdditionCalculator extends StatefulWidget {
  const AdditionCalculator({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<AdditionCalculator> createState() => _AdditionCalculator();
}

class _AdditionCalculator extends State<AdditionCalculator> {
  TextEditingController controllerNumber1 = TextEditingController();
  TextEditingController controllerNumber2 = TextEditingController();
  int sum = 0;

  void _recalculateNumber(String text) {
    setState(() {
      var number1 = int.tryParse(controllerNumber1.text) ?? 0;
      var number2 = int.tryParse(controllerNumber2.text) ?? 0;

      sum = number1 + number2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextFormField(
          controller: controllerNumber1,
          keyboardType: TextInputType.number,
          onChanged: _recalculateNumber,
        ),
        TextFormField(
          controller: controllerNumber2,
          keyboardType: TextInputType.number,
          onChanged: _recalculateNumber,
        ),
        Text('$sum'),
      ],
    );
  }
}
