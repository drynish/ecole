import 'package:flutter/material.dart';
import 'AdditionCalculator.dart';
import 'SubstractionCalculator.dart';
import 'MultiplicationCalculator.dart';
import 'DivisionCalculator.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyFullWidget(),
    );
  }
}

class MyFullWidget extends StatelessWidget {
  const MyFullWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Super Dooper Cool Calculator"),
        ),
        body: const Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Center(
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                    Expanded(
                      child: AdditionCalculator(title: 'Addition'),
                    ),
                    Expanded(
                      child: SubstractionCalculator(title: 'Substraction'),
                    )
                  ])
                ),
              ),
              Expanded(
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: MultiplicationCalculator(
                            title: 'Multiplication'),
                      ),
                      Expanded(
                        child: DivisionCalculator(title: "Division"),
                      )
                    ]
                  ),
                ),
              ),
            ]),
        )
    );
  }
}
