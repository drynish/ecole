import 'dart:async';
import 'package:flutter/material.dart';

class StopWatchApp extends StatelessWidget {
  const StopWatchApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Stopwatch'),
          ),
          body:StopWatch()
      )
    );
  }
}

class StopWatch extends StatefulWidget {
  const StopWatch({super.key});

  @override
  State createState() => StopWatchState();
}

class StopWatchState extends State<StopWatch> {
  late int seconds;
  late Timer timer;
  bool isTicking = true;

  String _secondsText() => seconds == 1 ? 'second' : 'seconds';

  @override
  void initState() {
    super.initState();

    seconds = 0;
  }

  void _onTick(Timer time) {
    setState(() {
      ++seconds;
    });
  }

  void _startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), _onTick);

    setState(() {
      isTicking = true;
    });
  }

  void _stopTimer() {
    timer.cancel();
    setState(() {
      isTicking = false;
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          '$seconds ${_secondsText()}',
          style: Theme.of(context).textTheme.headline5,
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty
                    .all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty
                    .all<Color>(Colors.white),
              ),
              onPressed: _startTimer,
              child: const Text('Start'),
            ),
            const SizedBox(width: 20),
            TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty
                    .all<Color>(Colors.red),
                foregroundColor: MaterialStateProperty
                    .all<Color>(Colors.white),
              ),
              onPressed: _stopTimer,
              child: const Text('Stop'),
            ),
          ],
        )
      ],
    );
  }
}