class DataItem {
  final String title;
  final String subtitle;
  final String url;

  const DataItem({
    required this.title,
    required this.subtitle,
    required this.url,
  });
}