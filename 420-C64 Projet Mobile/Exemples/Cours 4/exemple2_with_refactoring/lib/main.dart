import 'package:flutter/material.dart';
import 'dataview.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const title = 'Image Widget';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: MyContainerWidget(),
      ),
    );
  }
}

class MyContainerWidget extends StatelessWidget {
  MyContainerWidget({Key? key}) : super(key: key);

  final DataView data = DataView();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 180,
      color: Colors.black,
      child: Column(
        children: [
          Image.network(data.item.url),
          Text(
            data.item.title,
            style: const TextStyle(fontSize: 18, color: Colors.white),
          ),
          Text(
            data.item.subtitle,
            style: const TextStyle(fontSize: 14, color: Colors.grey),
          ),
        ],
      ),
    );
  }
}