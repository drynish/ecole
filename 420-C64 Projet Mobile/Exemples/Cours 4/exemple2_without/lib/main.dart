import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const title = 'Image Widget';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: Container(
          width: 200,
          height: 180,
          color: Colors.black,
          child: Column(
            children: [
              Image.network('https://oreil.ly/O4PEn'),
              const Text(
                'itemTitle',
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
              const Text(
                'itemSubTitle',
                style: TextStyle(fontSize: 13, color: Colors.grey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}