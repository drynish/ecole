import 'package:flutter/material.dart';

class ColorStream {
  late Stream colorStream;

  Stream<Color> getColors() async* {
    final List<Color> colors = [
      Colors.blueGrey,
      Colors.amber,
      Colors.deepPurple,
      Colors.lightBlue,
      Colors.teal
    ];

    yield colors[DateTime.now().second % 5];

    /*
    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      int index = t % 5;
      return colors[index];
    });
    */
  }

}