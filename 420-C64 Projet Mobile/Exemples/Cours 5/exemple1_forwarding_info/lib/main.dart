import 'package:flutter/material.dart';
import 'selection.dart';

void main() => runApp(const HomeScreen());

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Returning Data Demo'),
        ),
        // Create the SelectionButton widget in the next step.
        body: const Center(
          child: SelectionButton(),
        ),
      )
    );
  }
}
