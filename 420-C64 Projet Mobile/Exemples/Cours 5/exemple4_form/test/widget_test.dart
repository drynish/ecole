// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:exemple_3/login_screen.dart';

void main() {
  testWidgets("should allow login", (WidgetTester testWorker) async {
    // Arrange
    final testUsername = find.byKey(ValueKey("name"));
    final testEmail  = find.byKey(ValueKey("email"));
    final testLoginBtn  = find.byKey(ValueKey("LoginBtn"));

    // Act
    await testWorker.pumpWidget(LoginApp());
    await testWorker.enterText(testUsername, "username");
    await testWorker.enterText(testEmail, "email@email.com");
    await testWorker.tap(testLoginBtn);
    await testWorker.pump();

    // Assert
    expect(find.text("Hi username"), findsOneWidget);
  });
}
