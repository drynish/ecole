public class Calculatrice {

    // Méthode qui additionne deux entiers et retourne le résultat
    public static int additionner(int a, int b) {
        int somme = a + b;
        return somme;
    }

    // Méthode qui affiche un message (ne retourne rien)
    public static void afficherMessage(String message) {
        System.out.println(message);
    }

    public static void main(String[] args) {
        int resultat = additionner(5, 3); // Appel de la méthode additionner
        System.out.println("La somme est : " + resultat); // Affiche 8

        afficherMessage("Bonjour le monde !"); // Appel de la méthode afficherMessage
    }
}
