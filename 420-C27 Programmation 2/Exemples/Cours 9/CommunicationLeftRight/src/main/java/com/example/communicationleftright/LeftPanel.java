package com.example.communicationleftright;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class LeftPanel extends VBox {

    private MainWindow mainWindow;

    public LeftPanel(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        initialize();
    }

    private void initialize() {
        Button button1 = new Button("1");
        Button button2 = new Button("2");
        Button button3 = new Button("3");

        button1.setOnAction(e -> mainWindow.updateRightPanel("Contenu 1"));
        button2.setOnAction(e -> mainWindow.updateRightPanel("Contenu 2"));
        button3.setOnAction(e -> mainWindow.updateRightPanel("Contenu 3"));

        this.getChildren().addAll(button1, button2, button3);
    }
}
