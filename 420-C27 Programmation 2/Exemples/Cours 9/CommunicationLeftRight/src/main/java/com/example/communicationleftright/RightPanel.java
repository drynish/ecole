package com.example.communicationleftright;

import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public class RightPanel extends StackPane {

    private Label contentLabel;

    public RightPanel() {
        initialize();
    }

    private void initialize() {
        contentLabel = new Label("Contenu Initial");
        this.getChildren().add(contentLabel);
    }

    public void updateContent(String data) {
        contentLabel.setText(data);
    }
}