package com.example.communicationleftright;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainWindow extends Parent {

    private RightPanel rightPanel;

    public MainWindow(Stage stage) {
        BorderPane root = new BorderPane();

        LeftPanel leftPanel = new LeftPanel(this);
        rightPanel = new RightPanel();

        root.setLeft(leftPanel);
        root.setRight(rightPanel);

        Scene scene = new Scene(root, 640, 480);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public void updateRightPanel(String data) {
        rightPanel.updateContent(data);
    }

}
