module com.example.communicationleftright {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.communicationleftright to javafx.fxml;
    exports com.example.communicationleftright;
}