import java.io.Serializable;

public class Test  implements Serializable {

    Cours cours;
    int note;

    Test(Cours cours, int note) {
        this.cours = cours;
        this.note = note;
    }

    public Cours getCours() {
        return cours;
    }

    @Override
    public String toString() {
        return String.format("%s à %d%%", this.cours, this.note);
    }
}
