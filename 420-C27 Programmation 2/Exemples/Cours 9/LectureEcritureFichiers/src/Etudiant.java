import java.io.Serializable;
import java.util.ArrayList;

public class Etudiant implements Serializable {

    private String nom;
    private ArrayList<Cours> listCours;
    private ArrayList<Test> listTest;

    Etudiant(String nom) {
        this.nom = nom;
        listTest = new ArrayList<>();
        listCours = new ArrayList<>();
    }

    void addTest(Test test ) {
        if (listCours.contains(test.getCours())) {
            listTest.add(test);
        }
    }

    void addCours(Cours cours) {
        listCours.add(cours);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Test t : listTest)
            sb.append(String.format("%s a %s", this.nom, t.toString()));

        return sb.toString();
    }
}
