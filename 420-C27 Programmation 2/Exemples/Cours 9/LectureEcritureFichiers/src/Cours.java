import java.io.Serializable;

public class Cours implements Serializable {

    int id;
    String nom;

    Cours(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Override
    public String toString(){
        return String.format("%s", this.nom);
    }

}
