import java.io.*;

public class Main {
    public static void main(String[] args) {


        System.out.println("Hello world!");

        Etudiant michel = new Etudiant("Michel");

        Cours reseau1 = new Cours(1, "Réseau 1");
        michel.addCours(reseau1);

        Test exam1  = new Test(reseau1, 75);
        michel.addTest(exam1);

        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;
        try {
            fileOut =
                    new FileOutputStream("etudiant.dat");
            out = new ObjectOutputStream(fileOut);
            out.writeObject(michel);
            out.close();
            fileOut.close();
            System.out.println("Étudiant sauvegardé");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fileOut != null) {
                    fileOut.close();
                }
            } catch (IOException ex) {
                System.out.println("Erreur lors de la fermeture des flux : " + ex.getMessage());
            }
        }

        Etudiant michel2 = null;
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        try {
            fileIn = new FileInputStream("etudiant.dat");
            in = new ObjectInputStream(fileIn);
            michel2 = (Etudiant) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        } catch (ClassNotFoundException e) {
            System.out.println("Employee class not found");
            System.out.println(e.getMessage());
            return;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (fileIn != null) {
                    fileIn.close();
                }
            } catch (IOException ex) {
                System.out.println("Erreur lors de la fermeture des flux : " + ex.getMessage());
            }
        }

        System.out.println(michel2);
    }
}