import java.util.Random;

public enum Day {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;

    private static final Random RANDOM = new Random();

    public static Day randomDay() {
        int randomIndex = RANDOM.nextInt(values().length);
        return values()[randomIndex];
    }

    public static Day randomWeekday() {
        int randomIndex = RANDOM.nextInt(5); // 5 jours de travail
        return switch (randomIndex) {
            case 0 -> MONDAY;
            case 1 -> TUESDAY;
            case 2 -> WEDNESDAY;
            case 3 -> THURSDAY;
            case 4 -> FRIDAY;
            default -> throw new IllegalArgumentException("Invalid Index");
        };
    }
}

