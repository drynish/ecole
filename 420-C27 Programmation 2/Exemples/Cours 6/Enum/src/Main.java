public class Main {

    public void main(String[] Args) {
        System.out.println("Hello World");

        Day randomDay = Day.randomDay();
        System.out.printf("Jour aléatoire : %s\n\n", randomDay);

        Day randomWeekday = Day.randomWeekday();
        System.out.printf("Jour de travail aléatoire : %s", randomWeekday);
    }
}
