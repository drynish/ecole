package com.example.eventhandler2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        VBox vbox = new VBox();
        TextField txt1 = new TextField();
        TextField txt2 = new TextField();

        Button btn1 = new Button();
        Button btn2 = new Button();

        btn1.setText("Je suis le bouton le plus important");
        btn1.setDefaultButton(true);

        btn2.setText("Je suis le bouton le plus important");
        btn2.setDefaultButton(true);

        Label lbl = new Label();


        vbox.getChildren().addAll(txt1,txt2,lbl, btn1,btn2);

        EventHandler<ActionEvent> clickOnButton = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                try {
                    int a = Integer.parseInt(txt1.getText());
                    int b = Integer.parseInt(txt2.getText());

                    lbl.setText(String.valueOf(a+b));
                }
                catch (NumberFormatException e){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Erreur de transtypage");
                    alert.setContentText("Erreur de transtypage.");
                    alert.show();
                }
            }
        };

        btn1.setOnAction(clickOnButton);
        btn2.setOnAction(clickOnButton);



        Scene scene = new Scene( vbox, 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}