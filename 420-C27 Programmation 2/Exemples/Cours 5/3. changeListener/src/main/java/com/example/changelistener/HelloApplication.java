package com.example.changelistener;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;

import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Slider slider = new Slider(0, 100, 50);
        DoubleProperty sliderValue = slider.valueProperty();

        // Ajouter un ChangeListener à la propriété observable
        sliderValue.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                System.out.println("Valeur du slider modifiée : " + newValue);
            }
        });

        Scene scene = new Scene(slider, 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}