module com.example.baseProject {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.baseProject to javafx.fxml;
    exports com.example.baseProject;
}