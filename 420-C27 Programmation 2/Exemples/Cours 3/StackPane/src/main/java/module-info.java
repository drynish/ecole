module com.example.exemplejavafx2 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.exemplejavafx2 to javafx.fxml;
    exports com.example.exemplejavafx2;
}