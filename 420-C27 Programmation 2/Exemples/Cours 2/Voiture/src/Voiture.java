public class Voiture {

    // Attributs
    private String marque;
    private String modele;
    private int annee;
    private String couleur;
    private int vitesse; // Ajout de l'attribut vitesse

    // Constructeur
    public Voiture(String marque, String modele, int annee, String couleur) {
        this.marque = marque;
        this.modele = modele;
        this.annee = annee;
        this.couleur = couleur;
        this.vitesse = 0; // Initialisation de la vitesse à 0
    }

    // Méthodes
    public void demarrer() {
        System.out.println("La voiture démarre.");
    }

    public void accelerer() {
        System.out.println("La voiture accélère.");
        this.vitesse += 10; // Augmentation de la vitesse (exemple)
    }

    // Accesseur (getter) pour la vitesse
    public int getVitesse() {
        return this.vitesse;
    }

    // Mutateur (setter) pour la vitesse
    public void setVitesse(int vitesse) {
        if (vitesse >= 0) { // Validation de la vitesse (doit être positive ou nulle)
            this.vitesse = vitesse;
        } else {
            System.out.println("La vitesse ne peut pas être négative.");
        }
    }

}
