public class Tableau {

    private int[] data;

    public Tableau() {
        data = new int[0];
    }

    public Tableau(int a) {
        data = new int[1];
        data[0] = a;
    }

    public void ajouter(int a) {

        int[] new_data = new int[data.length+1];

        for(int i = 0 ; i < data.length ; i++) {
            new_data[i] = data[i];
        }

        // On remplace la chaîne
        data = new_data;

        // On ajoute le nouveau élément à la fin
        new_data[new_data.length - 1] = a;
    }

    public int length() {
        return data.length;
    }

    @Override
    public String toString() {
        StringBuilder retour = new StringBuilder();
        for (int i : data) {
            retour.append(String.format("%d\n", i));
        }

        return retour.toString();
    }
}
