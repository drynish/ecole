import static org.junit.jupiter.api.Assertions.*;

class TableauTest {

    @org.junit.jupiter.api.Test
    void ajouter1000 () {
        Tableau test = new Tableau();
        for (int i = 0 ; i < 1000; i++) {
            test.ajouter(i);
        }

        assertEquals(test.length(), 1000, "Les tableaux devraient avoir 1000 éléments");
    }

    @org.junit.jupiter.api.Test
    void ajouter10000 () {
        Tableau test = new Tableau();
        for (int i = 0 ; i < 10000; i++) {
            test.ajouter(i);
        }
        
        assertEquals(test.length(), 10000, "Les tableaux devraient avoir 10000 éléments");
    }

    @org.junit.jupiter.api.Test
    void ajouter100000 () {
        Tableau test = new Tableau();
        for (int i = 0 ; i < 100000; i++) {
            test.ajouter(i);
        }

        assertEquals(test.length(), 100000, "Les tableaux devraient avoir 100000 éléments");
    }

    @org.junit.jupiter.api.Test
    void ajouter500000 () {
        Tableau test = new Tableau();
        for (int i = 0 ; i < 500000; i++) {
            test.ajouter(i);
        }

        assertEquals(test.length(), 500000, "Les tableaux devraient avoir 500000 éléments");
    }
}