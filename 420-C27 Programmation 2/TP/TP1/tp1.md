# TP-1 - Gestionnaire d'Inventaire Personnel (IOU)

## Description

Ce projet consiste à développer un gestionnaire d'inventaire personnel (IOU - "I Owe You") pour organiser et suivre vos biens. Vous devrez définir et implémenter au moins trois catégories d'objets (par exemple: outils, livres, jeux, meubles) avec des attributs spécifiques à chaque catégorie.

## Fonctionnalités Principales

* **Gestion de fichiers multiples** : Possibilité de créer et gérer plusieurs fichiers d'inventaire distincts.
* **Catégories d'objets personnalisées** : Définition d'au moins trois types d'objets avec leurs propres caractéristiques.
* **Attributs communs pour chaque type d'objet** :
    * Nom ou Titre (Boîte de texte)
    * Prix (Float ou décimal)
    * Date d'achat (Calendrier)
    * Description : Générée dynamiquement en fonction du type d'objet.
    * Facture (Path) : Affichage uniquement dans la vue de création et de modification.
    * Emplacement (Boîte de texte) : Moi ou Autres?
    * État : Liste prédéfinie (En ma possession, Prêté, Perdu, etc.).
* **Attributs spécifiques aux catégories** :
    * Chaque catégorie d'objet aura des attributs supplémentaires pertinents: 
        * auteur et éditeur pour les livres, 
        * marque et modèle pour les outils, 
        * âge recommandé et nombre de joueurs pour les jeux.
    * Les interfaces d'ajout et de modification s'adapteront aux attributs de chaque catégorie en fonction du choix. Lors de la création d'un item, l'utilisateur doit choisir entre les 3 catégories avant l'insertion. Un coup inséré, le type d'objet est immuable (ne peut plus changer)
    * Utilisation de divers composants d'interface : Chaînes de caractères, nombres (entiers et flottants), sélecteurs de date, listes déroulantes, listes.

## Fonctionnalités Avancées

* **Recherche** : Recherche par mot-clé dans la descriptions générale et ou parmi les spécifiques des objets.
* **Filtres** :
    * Type d'objet (utilisation d'Enum).
    * Date d'achat: range (utilisation de deux calendriers).
    * Prix (comparaison).
    * État (utilisation d'Enum).

## Menu Fichier

* **Nouveau** : Création d'un nouveau fichier .dat vide.
* **Ouvrir** : Ouverture d'un fichier .dat existant avec gestion des erreurs d'importation.
* **Sauvegarder** : Sauvegarde des modifications dans le fichier .dat actuel.
* **Sauvegarder sous** : Sauvegarde dans un nouveau fichier .dat avec confirmation en cas d'écrasement.
* **Exporter** : Exportation des données au format texte (.txt).
* **Gestion des images de factures** : Stockage des images dans un sous-répertoire "images/" du répertoire du fichier .dat.


## Caractéristiques personnalisables

* **Identification différente et vues dédiées :**

    * Chaque type d'objet (livre, outil, jeu, etc.) aura ses propres attributs spécifiques.
    * L'interface d'affichage, d'insertion et de modification de chaque type d'objet sera gérée par une classe "View" dédiée (par exemple, `BookView`, `ToolView`, `GameView`).
    * Ces classes "View" contiendront des méthodes statiques pour créer et gérer les éléments d'interface utilisateur spécifiques à chaque type d'objet.
    * Par exemple :
        * `BookView.open(Book book, GridPane pane)` pour afficher les détails d'un livre.
        * `ToolView.add(Tool tool, GridPane pane)` pour ajouter un un nouvel outil.
        * `GameView.modify(Game game, GridPane pane)` pour modifier les détails existants d'un jeu.
    * Ces méthodes statiques prendront en paramètre l'objet à afficher, le `GridPane` dans lequel les éléments seront ajoutés.
    * Vous devez au minimum avoir des Strings, Nombres (entier ou flottant) DatePicker, des ComboBox, des ListView dans votre interface parmi les propriété intégrées ou ajoutées.

* **Séparation des préoccupations :**
    * Les classes de données (par exemple, `Book`, `Tool`, `Game`) contiendront uniquement les données et la logique métier.
    * Les classes "View" contiendront uniquement la logique d'affichage.

* **Respect des normes de programmation :**
    * Noms de variables et de méthodes significatifs.
    * Commentaires pour expliquer les décisions de conception et les fonctionnalités.
    * Gestion des erreurs et des exceptions.
    * https://github.com/Singcaster-CRLJ/CLJ/blob/main/Documents/Normes.md
    * https://github.com/Singcaster-CRLJ/CLJ/blob/main/Documents/Git.md

## Obligation

* Je ne veux pas d'ArrayList, je vois ce mot inscrit qqpart dans votre code et je vous mets 0.
* Vous devez utiliser des array [] pour votre programmation, je veux que les tableaux s'agrandissent à la hauteur de 10 espaces à la fois. Je veux que ce soit invisible pour l'utilisateur autant que possible:  On ne doit pas avoir des lignes dans une listview avec des trous dedans dans l'interface.
* Si vous supprimer des éléments ça ne réduit pas l'array, mais ça retire l'élément et décale les autres. On ne garde pas d'élément vide dans le tableau.
* Il n'y a pas de tri dans l'application, cependant certains éléments d'interface de JavaFX peut vous permettre de trier les éléments, je ne validerai pas cette fonctionnalité, mais lâchez vous lousse si vous avez envie.
* La sauvegarde est optimisée, ne sauvegarde pas d'array avec des lignes vides.


## Évaluation

| Critère                               | Valeur |
| :------------------------------------ | :----- |
| Affichage des données                 | 10     |
| Personnalisation des objets           | 10     |
| Ajout, modification, suppression      | 10     |
| Filtres, recherche                    | 10     |
| Sérialisation et gestion de fichiers  | 10     |
| Qualité de l'interface utilisateur    | 10     |
| Respect des normes de programmation   | 10     |
| Tests unitaires                       | 5      |
| **Total** | **75** |

## Vidéo

Vous devrez réaliser un vidéo qui montre le fonctionnement de votre application:
  * Ajout pour chaque type d'objet (points d'arrêts pour voir l'array et les éléments insérés)
  * Montrer la recherche
  * Montrer les filtres
  * Modification des trois objets précédemment rentré (points d'arrêts pour les éléments modifiés).
  * Montrer le changement dans les filtres.

**Ce document se veut une référence primordiale pour le TP toute modification du projet, tout oubli d'énoncé ou tout éclaircissement se fera à même le fichier. De grâce, ne téléchargez pas ce fichier sur votre poste car il ne sera pas mis à jour dans le futur. Vous pouvez le télécharger (clone) et faîtes des pull régulièrement.**

**Toute modification se fera par l'entremise de Discord ou de Mio**

**Gardez votre projet à long terme, il pourrait être intéressant pour un futur employeur, un futur ATE, un futur stage, ... démontrer nos talents nous permettent des fois de passer en avance sur d'autre candidat**

**Amusez-vou bien!!**