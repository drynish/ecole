# Classes et objets

## Présentation d'une classe

En Java, une classe est un peu comme un plan ou un modèle pour créer des objets. Imaginez un moule à gâteau : la classe est le moule, et les objets sont les gâteaux que vous faites avec ce moule. Chaque gâteau (objet) a une forme et des ingrédients (attributs) qui peuvent varier, mais ils suivent tous le même modèle (classe).

## Définition d'une classe

Une classe est un ensemble de données (attributs) et de comportements (méthodes) qui définissent un type d'objet.
Elle sert de modèle pour créer des instances de cet objet, appelées objets.

## Structure d'une classe

Une classe est composée de :

* Attributs (variables) : Les données qui décrivent l'objet.
* Méthodes (fonctions) : Les actions que l'objet peut effectuer.
* Constructeur : Une méthode spéciale utilisée pour initialiser les objets lors de leur création.

```java
public class Voiture {

    // Attributs
    String marque;
    String modele;
    int annee;
    String couleur;

    // Constructeur
    public Voiture(String marque, String modele, int annee, String couleur) {
        this.marque = marque;
        this.modele = modele;
        this.annee = annee;
        this.couleur = couleur;
    }

    // Méthodes
    public void demarrer() {
        System.out.println("La voiture démarre.");
    }

    public void accelerer() {
        System.out.println("La voiture accélère.");
    }
}
```

## Création d'objets

Pour créer un objet à partir d'une classe, on utilise le mot-clé **new** suivi du nom de la classe et des arguments du constructeur :

Par exemple

```java
Voiture maVoiture = new Voiture("Chevrolet", "Bolt", 2019, "Jaune");
```

Utilisation des objets

Une fois l'objet créé, on peut accéder à ses attributs et méthodes en utilisant l'opérateur point (.):

```java
System.out.println(maVoiture.marque); // Affiche "Chevrolet"
maVoiture.demarrer(); // Affiche "La voiture démarre."
```




## Modifions le code pour ajouter des concepts

```java
public class Voiture {

    // Attributs
    private String marque;
    private String modele;
    private int annee;
    private String couleur;
    private int vitesse; // Ajout de l'attribut vitesse

    // Constructeur
    public Voiture(String marque, String modele, int annee, String couleur) {
        this.marque = marque;
        this.modele = modele;
        this.annee = annee;
        this.couleur = couleur;
        this.vitesse = 0; // Initialisation de la vitesse à 0
    }

    // Méthodes
    public void demarrer() {
        System.out.println("La voiture démarre.");
    }

    public void accelerer() {
        System.out.println("La voiture accélère.");
        this.vitesse += 10; // Augmentation de la vitesse (exemple)
    }

    // Accesseur (getter) pour la vitesse
    public int getVitesse() {
        return this.vitesse;
    }

    // Mutateur (setter) pour la vitesse
    public void setVitesse(int vitesse) {
        if (vitesse >= 0) { // Validation de la vitesse (doit être positive ou nulle)
            this.vitesse = vitesse;
        } else {
            System.out.println("La vitesse ne peut pas être négative.");
        }
    }

    // Autres méthodes (getters et setters pour les autres attributs si nécessaire)
    // ...
}
```

## Attribut

Un entier (int) représentant la vitesse de la voiture. Il est déclaré private pour encapsuler l'attribut et contrôler son accès.

## Constructeur

Désormais dans le constructeur de Voiture, la vitesse est initialisée à 0 lors de la création d'une nouvelle voiture.

## Accesseur

Une méthode publique qui permet de lire la valeur de l'attribut vitesse depuis l'extérieur de la classe.

## Mutateur

Une méthode publique qui permet de modifier la valeur de l'attribut vitesse depuis l'extérieur de la classe. Il inclut une validation pour s'assurer que la vitesse ne soit pas négative.

## Concepts

Encapsulation : Les attributs sont généralement privés (accessibles uniquement à l'intérieur de la classe) et les méthodes publiques (accessibles depuis l'extérieur). Cela permet de protéger les données et de contrôler leur accès.


## Instancions un objet

```java
Voiture maVoiture = new Voiture("Chevrolet", "Bolt", 2019, "Jaune");
System.out.println("Vitesse initiale : " + maVoiture.getVitesse()); // Affiche 0

maVoiture.accelerer();
System.out.println("Vitesse après accélération : " + maVoiture.getVitesse()); // Affiche 10

maVoiture.setVitesse(50);
System.out.println("Vitesse après modification : " + maVoiture.getVitesse()); // Affiche 50

maVoiture.setVitesse(-10); // Tentative de vitesse négative
System.out.println("Vitesse après tentative de vitesse négative : " + maVoiture.getVitesse()); // Affiche 50 (la vitesse n'a pas été modifiée)
```

## En résumé

* Les classes sont les éléments de base de la programmation orientée objet en Java.
* Elles permettent de définir des objets avec des attributs et des comportements spécifiques.
* Les classes favorisent la réutilisabilité du code, l'encapsulation des données et la modularité du programme.




Une classe est la définition d'un type de données, on y définit:

* données membres
* méthodes
* constructeurs
* accesseurs (get) et mutateurs (set)
* gestion de la classe

## Portée
En programmation, la portée détermine qui peut faire appel à une classe, à une donnée membre d'une classe ou à une méthode d'une classe.

* public accessible à toutes les classes du package
* private accessible à l'intérieur de la classe

![](img/Classe2.png)

## Instanciation (création d'un objet d'une classe)

![](img/Classe3.png)

## Surcharge du constructeur

![](img/Classe4.png)

## Mot clé static

* La POO permet d'instancier un objet d'une classe. Il existe des situations où des données et des méthodes n'ont pas à être instanciées dans un objet.
* Le mot réservé "static" permet à une données d'être déclarée en mémoire sans création d'objets de sa classe.
* Le mot réservé "static" permet à une méthode d'être déclarée et utilisée sans création d'objets de sa classe.
* On accède aux données "static" directement plutôt qu'avec un objet.

![](img/Classe5.jpg)



